
UNAME := $(shell uname)
SDK_CPP = sdk/cpp

ifeq ($(UNAME), Linux)
	LDFLAGS += -lm
	ifndef EXE
		EXE = a
	endif
	LIB = sdk_lin
else
	LDFLAGS += -lm -lws2_32 -lwsock32
	ifndef EXE
		EXE = a.exe
	endif
	LIB = sdk_win
endif

CXXFLAGS += -O3 -std=c++17 -s
#CXXFLAGS += -Wall -Wextra -Wshadow
#CXXFLAGS += -Wformat=2 -Wfloat-equal -Wlogical-op -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC -D_FORTIFY_SOURCE=2 
CXXFLAGS += -I$(SDK_CPP) -Istrategy
CXXFLAGS += -D_LOCALx -D_TIMINGx -D_FASTx
ifdef VIS
	CXXFLAGS += -D_VIS
endif

.PHONY: all lib

$(EXE): lib$(LIB).a strategy/*.cpp  strategy/*.h Makefile
	g++ $(CXXFLAGS) strategy/*.cpp -L. -l$(LIB) -o $(EXE) $(LDFLAGS)

lib$(LIB).a:
	g++ $(CXXFLAGS) $(SDK_CPP)/*.cpp $(SDK_CPP)/model/*.cpp -c -s
	ar rcs lib$(LIB).a *.o
	rm *.o
