#ifndef _SOLVER_H_
#define _SOLVER_H_

#include <map>
#include "world.h"
#include "action.h"

namespace strategy {

struct Solver
{
	void solve(const World &world);
	std::map<int,Action> actions;
};

}; // strategy

#endif // _SOLVER_H_
