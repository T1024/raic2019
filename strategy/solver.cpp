#include <assert.h>
#include <functional>
#include <algorithm>
#include "utils.h"
#include "action_plan.h"
#include "solver.h"

namespace strategy {

struct StarterStrategy
{
	//ActionSequence best_plan;
	AllUnitActions best_plans;
	std::vector<PathFinder> heal_paths;

	int scores[2] = {0,0};
	bool agressive_mode = false;
	int last_score_change = 0;

	const real min_fire_dist = 8.0;
	//const WeaponType weapon_priority[2] = {WeaponType::RIFLE, WeaponType::PISTOL};
	const WeaponType weapon_priority[2] = {WeaponType::PISTOL, WeaponType::BAZUKA};

	
	StarterStrategy()
	{
	}

	template<class T>
	const T *get_closest_object(const std::vector<T> &objs, const World &world, const Unit &me, bool anyway = true, const std::function<bool (const T&)> filter = [](const T &obj) {return true;})
	{
		const T *res = 0, *res2 = 0;
		for(const T &obj : objs) if(filter(obj))
		{
			PathFinder pf(world, me.get_cell_of(obj.x), me.get_cell_of(obj.y));

			bool ok = true;
			for(auto &enemy : world.units) if(!enemy.is_teammate)
				if(pf.get_dist_from(enemy) < pf.get_dist_from(me))
					ok = false;

			if(ok && (!res || obj.dist2(me) < res->dist2(me)))
				res = &obj;

			if(anyway && !res && (!res2 || obj.dist2(me) < res2->dist2(me)))
				res2 = &obj;
		}

		return (res ? res : res2);
	}

	ActionSequence gen_plan(const World &world, int unit_idx)
	{
		ActionSequence plan(world, unit_idx);

		TimedAction action(Action(),0);
		//int diry = rand()%2;
		action.jump = rand()%2;
		action.jump_down = !action.jump && rand()%2;
		action.vel = world.unit_max_speed*(rand()%2*2-1);
		action.tick = world.tick + rand()%60;
		plan.add(action);

		action.jump = rand()%2;//!action.jump;
		action.jump_down = !action.jump && rand()%2;
		action.vel = world.unit_max_speed*(rand()%2*2-1);
		action.tick += 1 + rand()%60;
		plan.add(action);

		action.jump = rand()%2;//!action.jump;
		action.jump_down = !action.jump && rand()%2;
		action.vel = world.unit_max_speed*(rand()%2*2-1);
		action.tick += 1 + rand()%60;
		plan.add(action);

		action.jump = false;
		action.tick = world.tick + 999;
		plan.add(action);

		return plan;
	}

	Action DriveEnemy(const World &world, const Unit &unit)
	{
		int best_i = -1;
		real min_d = 999999;
		for(int i=0; i<world.heals.size(); ++i)
		{
			real d = heal_paths[i].get_dist_from(unit);
			if(d < min_d)
			{
				min_d = d;
				best_i = i;
			}
		}
		Action action;

		if(best_i == -1)
			return action;

		Pos pleft = unit - Vec(0.3,0.0);
		Pos pright = unit + Vec(0.3,0.0);
		Pos ptop = unit + Vec(0.0,0.3);
		Pos pbottom = unit - Vec(0.0,0.3);
		real dleft = heal_paths[best_i].get_dist_from(pleft);
		real dright = heal_paths[best_i].get_dist_from(pright);
		real dtop = heal_paths[best_i].get_dist_from(ptop);
		real dbottom = heal_paths[best_i].get_dist_from(pbottom);
		if(dleft < min_d-EPS && dleft < dright-EPS)
			action.vel = -10.0;
		else if(dright < min_d-EPS && dright < dleft-EPS)
			action.vel = 10.0;

		// dprint(action.vel, min_d, dleft, dright);
		// dprint(unit, pleft, pright);

		if(EPS < fabs(action.vel))
		{
			const Unit *blocker = 0;
			for(auto &me : world.units) if(me.is_teammate && 0.0 < action.vel * (me.x - unit.x))
			{
				if(!blocker || unit.dist(me) < unit.dist2(*blocker))
					blocker = &me;
			}

			if(blocker && fabs(blocker->y - unit.y) < unit.size.y && fabs(blocker->x - unit.x) < 2.0)
				action.jump = true;
			else if(world.cells[unit.get_corner_celly(-1.0)][unit.get_corner_cellx(sign(action.vel)*1.1)] == ObjectType::WALL)
				action.jump = true;
		}

		if(dtop < min_d-EPS && dtop < dbottom-EPS)
			action.jump = true;
		else if(dbottom < min_d-EPS && dbottom < dtop-EPS)
			action.jump_down = true;

		return action;
	}

	struct UnitResult
	{
		int target_reach_time = 9999;
		int shoot_tick = 9999;
		real min_dist = 999999.0;
		int health = -1;
		Action action0;

		bool suicide = false;
	};

	struct SimulationResult
	{
		real score = -1;

		std::vector<real> heal_ownership;
		Unit my_future_pos;
		Unit his_future_pos;

		UnitsData<UnitResult> u;

		UnitResult &operator[] (const Unit &unit) { return u[unit]; }
	};
	SimulationResult sim_res;

	UnitsData<Object> targets;
	UnitsData<Unit> fire_targets;
	UnitsData<PathFinder> targets_pf;
	UnitsData<bool> is_fire_target;

	int my_team_size;

	real evaluate_plan(const World &world0, const Unit &me0, AllUnitActions &plan, SimulationResult &res)
	{
		FUNCTION_STAT
		World world = world0;
		Unit &me = world[me0.idx];
		Unit &fire_target = world[fire_targets[me].idx];

		bool controll_heal = !agressive_mode;
		std::vector<real> heal_ownership(world0.heals.size(), 1.0);

		real score = 0.0;
		for(const Unit &unit : world.units)
		{
			if(!unit.is_teammate)
				plan(unit.id).reset(world);	
			plan(unit.id).start_iter();
		}

		bool prev_on_ground = me0.on_ground;
		real q = 1.0;
		for(int t=0; t<60; ++t)
		{
			q *= 0.90;

			if(t < 30)
			{
				for(auto &enemy : world.units) if(!enemy.is_teammate && enemy.health <= 50)
					plan(enemy.id).add(TimedAction(DriveEnemy(world, enemy), world.tick));
			}
			world.simulate_tick(plan);
			if(me.health <= 0)
				continue;

			if(is_intersect(me, targets[me], 0.5-EPS, 0.5-EPS))
			{
				res[me].target_reach_time = std::min(res[me].target_reach_time, t);
			}

			real d = targets_pf[me].get_dist_from(me);
			res[me].min_dist = std::min(res[me].min_dist, d + t/12.0);

			if(controll_heal && t == 15)
			{
				for(int i=0; i<world.heals.size(); ++i)
				{
					real d1 = 9999.0, d2 = 9999.0;
					for(const Unit &enemy : world.units)
						if(enemy.is_teammate)
							d1 = std::min(d1, heal_paths[i].get_dist_from(enemy));
						else
							d2 = std::min(d2, heal_paths[i].get_dist_from(enemy));
					real ownership = 1.0;
					if(fabs(d1 - d2) < 1.0)
						ownership = 0.0;
					else if(d2 < d1)
						ownership = -1.0;
					heal_ownership[i] = std::min(heal_ownership[i], ownership);
				}
			}

 			int tmp_fire_tick_left = me.weapon.fire_tick_left;
 			if(599 < me.weapon.fire_tick_left)
 				me.weapon.fire_tick_left = 0;
			Action tmp;
			if(prev_on_ground && do_suicide(world, me, tmp))
			{
				world.explode(me, 3.0, me.nmine*50, world.units, 1);
				score += 500000 * q;
				res[me].suicide = world.tick;
			}
			else
			{
				for(auto &enemy : world.units) if(!enemy.is_teammate && me.health <= enemy.nmine*50 && 0 < enemy.health && enemy.on_ground && enemy.weapon.fire_tick_left <= 0 && world.scores[0] <= world.scores[1] + me.health)
				{
					Pos mine_pos = enemy - Vec(0, enemy.size.y/2 - 0.25);
					real safe_radius = 3.0;// - std::max(0, enemy.weapon.fire_tick_left) * (World::unit_max_speed*0.5/60);
					int nmy = 0;
					for(auto &me : world.units) if(me.is_teammate)
					{
						if(is_intersect(me, mine_pos, safe_radius, safe_radius))
						{
							++nmy;
						}
					}
					if(my_team_size <= nmy)
					{
						world.explode(mine_pos, 3.0, enemy.nmine*50, world.units, 1);
						//score -= 50000.0*q;
					}
				}
			}
			me.weapon.fire_tick_left = tmp_fire_tick_left;

			if(!agressive_mode)
			if(fire_target.has_weapon() && fire_target.weapon.fire_tick_left <= 0 && me.dist2(fire_target) < SQR(min_fire_dist) && is_fire_target[me])
			{
				fire_target.weapon.fire_tick_left = 999;
				real dd = me.dist(fire_target);
				real profit = (min_fire_dist - dd + 0.9876) * q * (world.heals.empty() ? 25.0 : 20.0);
				profit *= 1.6;
				if(me.jump_speed < 15.0)
					profit *= (1.0 - clamp(me.jump_tick_left, 0, 10)*0.05);
				score -= profit;
			}

			if(!agressive_mode)
			for(Unit &me : world.units) if(me.is_teammate)
			{
				Unit &fire_target = world[fire_targets[me].idx];

				if(me.has_weapon() && me.weapon.fire_tick_left <= 0 && me.dist2(fire_target) < SQR(min_fire_dist)) // TODO: get_first_intersect_wall?
				{
					if(my_team_size == 2)
					{
						// Check friendly fire
						Vec aim_dir = (fire_target - me).norm();
						const AngleRange target_range = get_view_range(me, fire_target, aim_dir);
						real range = target_range.len();
						bool ok = true;
						for(const Unit &buddy : world.units) if(buddy.is_teammate && buddy.id != me.id && 0.0 < aim_dir * (buddy - me))
						{
							if(me.weapon.weapon_type == WeaponType::BAZUKA && fire_target.dist2(buddy) < SQR(3.0)) {
								ok = false;
								break;
							}

							if(me.dist2(fire_target) < me.dist2(buddy))
								continue;
							AngleRange friend_range = get_view_range(me, buddy, aim_dir);
							friend_range.fit(target_range);
							real common_len = friend_range.get_common_len(target_range);
							if(EPS < common_len)
								ok = false;
						}

						if(!ok)
							continue;
					}

					res[me].shoot_tick = world.tick - 1;
					me.weapon.fire_tick_left = 999;

					if(is_fire_target[me])
					{
						real dd = me.dist(fire_target);
						real profit = (min_fire_dist - dd + 0.9876) * q * 20.0;
						score += profit;
					}
				}
			}

			if(is_fire_target[me])
			{
				for(const Unit &buddy : world.units) if(buddy.is_teammate && buddy.id != me.id && is_fire_target[buddy])
				{
					score += std::min(4.0, me.dist(buddy)) * 0.1 * q;
				}
			}

			for(auto &mine : world.mines) if(is_intersect(me, mine, mine.exp_radius, mine.exp_radius))
			{
				score -= 100.0*q;
				if(MineState::TRIGGERED <= mine.state)
					score -= 1000.0*q;
			}

			if(30 <= t)
			{
				bool finish = true;
				for(Unit &me : world.units) if(me.is_teammate)
				{
					finish &= is_fire_target[me];
				}
				if(finish)
					break;
			}

			prev_on_ground = me.on_ground;
		}

		for(Unit &unit : world.units)
		{
			real p = (unit.is_teammate ? 1.0 : -0.8);
			res[unit].health = unit.health;
			if(0 < unit.health)
				score += unit.health * 100.0 * p;
			else
				score -= 100 * 100.0 * p;
		}

		if(res[me].target_reach_time < 999 && !is_fire_target[me])
		{
			score -= res[me].target_reach_time*0.1;
		}
		else
		{
			score -= 10.0 + res[me].min_dist;
		}

		if(controll_heal && me0.has_weapon() && is_fire_target[me])
		{
			for(int i=0; i<world.heals.size(); ++i) if(world.heals[i].valid)
			{
				assert(-1.0-EPS < heal_ownership[i] && heal_ownership[i] < 1.0+EPS);
				score += heal_ownership[i] * 100.0;// * 3 * pow(0.9, heal_paths[i].get_dist_from(me0));// * 45;
			}
		}

		res.heal_ownership = heal_ownership;
		res.score = score;

		return score;
	}


	bool go_to(const World &world, const Unit &me)
	{
		FUNCTION_STAT

		AllUnitActions curr_plan = best_plans;
		AllUnitActions best_plan0 = best_plans;
		real best_score = -999999, score0 = -1;
		int niter = fast_mode ? 19 : 99;
		for(int iter=0; iter<niter; ++iter)
		{
			if(iter != 0)
				curr_plan(me.id) = gen_plan(world, me.idx);

			SimulationResult sr;
			real score = evaluate_plan(world, me, curr_plan, sr);

			if(iter == 0 || best_score+EPS < score)
			{
				best_score = score;
				best_plans = curr_plan;
				sim_res = sr;
			}

			if(iter == 0)
				score0 = score;
		}

		Action &action = sim_res[me].action0;
		action.vel = best_plans(me.id).get(world.tick).vel;
		action.jump = best_plans(me.id).get(world.tick).jump;
		action.jump_down = best_plans(me.id).get(world.tick).jump_down;

		dprint(best_score);
#ifdef _VIS
	real x0 = (me.local_idx == 0 ? 0.0 : 8.0), y0 = 0.0;

	char tmp[99];
	real y = 31;
	sprintf(tmp, "score: %.3f -> %.3lf\n", score0, best_score);
	draw_text(Vec(-2.0,y) + Vec(x0,y0), tmp, 0, 16.0, {1.0,1.0,1.0,1.0});
	y -= 1.0;

	if(me.local_idx == 0)
	{
		std::string text ="hp:"; 
		for(auto unit : world.units)
		{
			sprintf(tmp, " %d", sim_res[unit].health);
			text += tmp;
		}
		draw_text(Vec(-2.0,y) + Vec(x0,y0), text, 0, 16.0, {1.0,1.0,1.0,1.0});
	}
	y -= 1.0;

	sprintf(tmp, "fire... %d\n", me.weapon.fire_tick_left);
	draw_text(Vec(-2.0,y) + Vec(x0,y0), tmp, 0, 16.0, {1.0,1.0,1.0,1.0});
	y -= 1.0;

	sprintf(tmp, "shoot... %d\n", sim_res[me].shoot_tick);
	draw_text(Vec(-2.0,y) + Vec(x0,y0), tmp, 0, 16.0, {1.0,1.0,1.0,1.0});
	y -= 1.0;

	World w = world;
	Pos prev = w[me.idx];
	for(const Unit &unit : world.units)
		best_plan0(unit.id).start_iter();
	for(int t=0; t<30; ++t)
	{
		//Vec size(0.05,0.05);
		w.simulate_tick(best_plan0);
		//draw_rect(w[me.idx]-size*0.5, size, {0.0,0.0,1.0,1.0});
		draw_line(prev, w[me.idx], 0.2, {0.0,0.0,1.0,1.0});
		prev = w[me.idx];
	}

	w = world;
	for(const Unit &unit : world.units)
		best_plans(unit.id).start_iter();
	for(int t=0; t<30; ++t)
	{
		Vec size(0.1,0.1);
		w.simulate_tick(best_plans);

		if(me.local_idx == 0)
		{
			for(auto &enemy : w.units) if(!enemy.is_teammate)
				draw_rect(enemy-size*0.5, size, {1.0, 0.0, 0.0, 1.0});
		}

		bool shoot_now = sim_res[me].shoot_tick == w.tick-1;
		if(shoot_now)
			size *= 3;
		draw_rect(w[me.idx]-size*0.5, size, {1.0,(shoot_now ? 0.0 : 1.0),1.0,1.0});
	}

	Pos target = targets[me];
	draw_line(Vec(target.x-0.5,target.y-0.5), Vec(target.x+0.5,target.y+0.5), 0.1, {0.0,1.0,0.0,0.5});
	draw_line(Vec(target.x+0.5,target.y-0.5), Vec(target.x-0.5,target.y+0.5), 0.1, {0.0,1.0,0.0,0.5});

	// Pos p1 = sim_res.my_future_pos;
	// Pos p2 = sim_res.his_future_pos;
	// draw_line(p1 - Vec(1,0), p1 + Vec(1,0), 0.2, {1.0, 0.0, 0.0, 1.0});
	// draw_line(p1 - Vec(0,1), p1 + Vec(0,1), 0.2, {1.0, 0.0, 0.0, 1.0});
	// draw_line(p2 - Vec(1,0), p2 + Vec(1,0), 0.2, {0.0, 1.0, 0.0, 1.0});
	// draw_line(p2 - Vec(0,1), p2 + Vec(0,1), 0.2, {0.0, 1.0, 0.0, 1.0});

	for(const Mine &mine: world.mines)
	{
		char tmp[99];
		sprintf(tmp, "timer... %d\n", mine.timer);
		draw_text(Vec(-2.0,29.0), tmp, 0, 16.0, {1.0,1.0,1.0,1.0});

		std::array<real,4> colors[4] = {
			std::array<real,4>({0.0, 1.0, 0.0, 0.5}),
			std::array<real,4>({1.0, 1.0, 0.0, 0.5}),
			std::array<real,4>({0.0, 1.0, 1.0, 0.5}),
			std::array<real,4>({0.0, 0.0, 0.0, 0.5}),
		};
		std::array<real,4> color = colors[(int)mine.state];

		draw_rect(mine - mine.size*0.5, mine.size, color);
		draw_line(mine - Vec(mine.exp_radius, mine.exp_radius), mine + Vec(-mine.exp_radius, mine.exp_radius), 0.2, color);
		draw_line(mine + Vec(-mine.exp_radius, mine.exp_radius), mine + Vec(mine.exp_radius, mine.exp_radius), 0.2, color);
	}

	// for(int i=0; i<world.heals.size(); ++i) if(world.heals[i].valid)
	// {
	// 	std::array<real,4> colors[3] = {
	// 		std::array<real,4>({1.0, 0.0, 0.0, 0.5}),
	// 		std::array<real,4>({1.0, 1.0, 0.0, 0.5}),
	// 		std::array<real,4>({0.0, 1.0, 0.0, 0.5})
	// 	};
	// 	auto color = colors[(int)round(sim_res.heal_ownership[i] + 1)];
	// 	draw_rect(world.heals[i] - (world.heals[i].size*1.2)*0.5, world.heals[i].size*1.2, color);
	// }

#endif
		return true;
	}


	const Unit *get_fire_target(const World &world, const Unit &me)
	{
		const Unit *target_unit = 0;
		real min_d = 999999.0;
		for(auto &enemy : world.units) if(!enemy.is_teammate)
		{
			real d = me.dist2(enemy);
			if(d < min_d)
			{
				target_unit = &enemy;
				min_d = d;
			}
		}
		assert(target_unit);
		return target_unit;
	}


	real shoot_score = 0.0;

	real calc_optimal_shoot_angle(const World &world, const Unit &me, const Unit &target_unit)
	{
		shoot_score = 0.0;
		if(!me.has_weapon())
			return 0.0;

		if(fast_mode)
		{
			shoot_score = 999.0;
			return calc_optimal_shoot_angle2(world, me, target_unit);
		}

		FUNCTION_STAT

		if(10.0 < me.dist(fire_targets[me]))
		{
			shoot_score = 1.0;
			return calc_optimal_shoot_angle2(world, me, target_unit);
		}

		shoot_score = 0.0;
		Vec target_dir = (target_unit - me).norm();
		real tangle = atan2(target_dir.y, target_dir.x);

		std::vector<Bullet> bullets0;
		std::vector<real> angles;
		std::vector<int> last_ticks;
		auto &params = me.weapon_params();
		real delta = me.weapon.weapon_type == WeaponType::BAZUKA ? 2*PI/180.1 : PI/180.1;
		real range = me.weapon.weapon_type == WeaponType::BAZUKA ? 2*PI/3 : PI/3;
		for(real angle = tangle-range; angle<tangle+range; angle += delta)
		{
			Vec dir(cos(angle), sin(angle));
			Bullet b(me, Vec(params.bullet_size, params.bullet_size), dir*params.bullet_speed, me.weapon.weapon_type, params.damage, me.id);
			b.exp_damage = params.exp_damage;
			b.exp_size = params.exp_size;
			bullets0.push_back(b);
			angles.push_back(angle);
			last_ticks.push_back(-1);
		}

		std::vector< std::vector<int> > last_hit(world.units.size(), std::vector<int> (bullets0.size(), -1));
		std::vector< std::vector<real> > min_dmg(world.units.size(), std::vector<real> (bullets0.size(), 999999.0));

		World w1;
		const int niter = 20;
		const int nsub_tick = 5;
		for(int iter=0; iter<niter; ++iter)
		{
			std::vector<Bullet> bullets = bullets0;
			std::vector<bool> finished(bullets.size(), false);
			std::vector< std::vector<real> > dmgs(world.units.size(), std::vector<real> (bullets0.size(), 0.0));
			int nfinished = 0;

			World w = world;
			AllUnitActions plans(w);
			auto plan = gen_plan(w, -1);
			for(auto &unit : w.units)
				plans.set(unit.id, plan);

			for(int t=0; t<60 && nfinished < bullets0.size(); ++t)
			{
				for(int iu=0; iu<w.units.size(); ++iu)
				{
					Unit &unit = w.units[iu];
					w1.tick = world.tick + t;
					bool tmp = unit.is_teammate;
					unit.is_teammate = true;
					unit.idx = 0;
					w1.units.push_back(unit);
					w1.simulate_tick(plans);
					unit = w1.units[0];
					unit.idx = iu;
					unit.is_teammate = tmp;
					w1.units.pop_back();
				}

				for(int sub=0; sub<nsub_tick; ++sub)
				{
					for(int ib=0; ib<bullets.size(); ++ib) if(!finished[ib])
					{
						Bullet &b = bullets[ib];
						if(last_ticks[ib] != -1)
						{
							finished[ib] = last_ticks[ib] == t*nsub_tick + sub;
						}
						else
						{
							int iy1 = b.get_corner_celly(-1);
							int iy2 = b.get_corner_celly(1);
							int ix1 = b.get_corner_cellx(-1);
							int ix2 = b.get_corner_cellx(1);
							for(int iy=iy1; iy<=iy2; ++iy)
								for(int ix=ix1; ix<=ix2; ++ix)
									if(world.cells[iy][ix] == strategy::ObjectType::WALL)
									{
										finished[ib] = true;
									}
						}
						if(finished[ib]) {
							last_ticks[ib] = t*nsub_tick + sub;
							++nfinished;

							if(b.exp_damage)
							for(int iu=0; iu<w.units.size(); ++iu)
							{
								Unit &unit = w.units[iu];
								if(is_intersect(unit, b, b.exp_size, b.exp_size))
								{
									dmgs[iu][ib] += b.exp_damage;
								}
							}
							continue;
						}

						b += b.vel * (1.0/(60.0*nsub_tick));
						for(int iu=0; iu<w.units.size(); ++iu)
						{
							Unit &unit = w.units[iu];

							if(unit.id != me.id && last_hit[iu][ib] != iter && is_intersect(b, unit, EPS))
							{
								last_hit[iu][ib] = iter;
								dmgs[iu][ib] += b.damage;
							}
						}
					}
				}
			}

			for(int iu=0; iu<w.units.size(); ++iu)
				for(int ib=0; ib<bullets.size(); ++ib)
					min_dmg[iu][ib] = std::min(dmgs[iu][ib], min_dmg[iu][ib]);
		}

		real best_score = -999999.0, best_rot = 0.0;
		real angle1 = calc_optimal_shoot_angle2(world, me, target_unit);

		for(int irot=-180; irot<180; irot += 1)
		{
			real rot = irot*PI/180;
			real new_spread = std::min(me.weapon_params().max_spread, me.weapon.spread + std::abs(rot));
			AngleRange rnew(me.weapon.last_angle+rot - new_spread, me.weapon.last_angle+rot + new_spread);

			real score = 0.0;
			for(int ib=0; ib<bullets0.size(); ++ib)
			{
				if(rnew.contain(angles[ib]))
				{
					for(int iu=0; iu<world.units.size(); ++iu)
					{
						score += min_dmg[iu][ib] * (world[iu].is_teammate ? -1.0 : 1.0);
						if(world[iu].is_teammate && iu != me.idx && 25.0 < min_dmg[iu][ib])
							score -= 9999.0;
					}
				}
			}
			//if(fabs(angle_diff(angle1, me.weapon.last_angle+rot)) < 2*PI/180)
			//	score += 0.01;

			score *= delta / rnew.len();

			if(best_score < score)
			{
				best_score = score;
				best_rot = rot;
			}
		}

		shoot_score = best_score;

		real angle = me.weapon.last_angle + best_rot;
		if(best_score < EPS)
			angle = angle1;

#ifdef _VIS
		for(int ib=0; ib<bullets0.size(); ++ib)
		{
			real score = 0.0;
			for(int iu=0; iu<world.units.size(); ++iu)
			{
				score += min_dmg[iu][ib] * (world[iu].is_teammate ? -1.0 : 1.0);
			}
			if(EPS < fabs(score))
			{
				real angle = angles[ib];
				Vec dir(cos(angle), sin(angle));
				draw_line(me, me + dir*20,  0.1, {(0 < score ? 1.0 : 0.0), 0.0, 0.0, 0.3});
			}
		}

		draw_line(me, me + Vec(cos(angle), sin(angle))*20,  0.07, {1.0, 1.0, 1.0, 1.0});

		real x0 = (me.local_idx == 0 ? 0.0 : 8.0), y0 = 0.0;
		char tmp[99];
		sprintf(tmp, "shoot_score: %.3f\n", shoot_score);
		draw_text(Vec(-2.0,25.0) + Vec(x0,y0), tmp, 0, 16.0, {1.0,1.0,1.0,1.0});
#endif
		return angle;
	}

	real calc_optimal_shoot_angle2(const World &world, const Unit &me, const Unit &target_unit)
	{
		FUNCTION_STAT
		real bullet_speed = me.weapon_params().bullet_speed;
		int dt = round(60 * me.dist(target_unit) / bullet_speed);

		World w = world;
		w.units.clear();

		Action act;
		act.jump = true;
		Unit target1 = target_unit;
		target1.is_teammate = true; // TODO: hack
		for(int t=0; t<dt; ++t)
		{
			w.apply_action_to_unit(target1, act);
			w.move_unit(target1, 1, act);
		}

		act.jump = false;
		act.jump_down = true;
		Unit target2 = target_unit;
		target2.is_teammate = true; // TODO: hack
		for(int t=0; t<dt; ++t)
		{
			w.apply_action_to_unit(target2, act);
			w.move_unit(target2, 1, act);
		}

		Unit target = target_unit;
		target.y = (target1.y + target2.y) / 2.0;

		return calc_optimal_shoot_angle1(world, me, target);
	}

	real calc_optimal_shoot_angle1(const World &world, const Unit &me, const Unit &target_unit)
	{
		// Pos from = sim_res.my_future_pos;
		// Unit to = sim_res.his_future_pos;
		Pos from = me;
		Unit to = target_unit;

		Vec target_dir = (to - from).norm();
		real tangle = atan2(target_dir.y, target_dir.x);
		Range rv = get_view_range(from, to, target_dir);
		AngleRange rview(rv.low+tangle, rv.high+tangle);
		AngleRange rshot(me.weapon.last_angle - me.weapon.spread, me.weapon.last_angle + me.weapon.spread);

		real best_cover = -999.0;
		real best_rot = -999.0;
		for(int irot=-180; irot<180; irot += 1)
		{
			real rot = irot*PI/180;
			real new_spread = std::min(me.weapon_params().max_spread, me.weapon.spread + std::abs(rot));
			AngleRange rnew(me.weapon.last_angle+rot - new_spread, me.weapon.last_angle+rot + new_spread);
			real cover = rnew.get_common_len(rview) / rnew.len();
			if(best_cover+1E-3 < cover || (best_cover-1E-3 < cover && std::abs(rot) < std::abs(best_rot)))
			{
				best_cover = cover;
				best_rot = rot;
			}
		}

		real angle = me.weapon.last_angle + best_rot;

		real new_spread = std::min(me.weapon_params().max_spread, me.weapon.spread + std::abs(best_rot));
		Range rnew;// = rview.fit(rshot);
		rnew.low = angle - new_spread;
		rnew.high = angle + new_spread;

		// draw_line(from, from + Vec(cos(rview.low), sin(rview.low))*20,  0.07, {1.0, 1.0, 1.0, 1.0});
		// draw_line(from, from + Vec(cos(rview.high),sin(rview.high))*20, 0.07, {1.0, 1.0, 1.0, 1.0});
		// draw_line(from, from + Vec(cos(rshot.low), sin(rshot.low))*20,  0.1, {1.0, 0.0, 0.0, 1.0});
		// draw_line(from, from + Vec(cos(rshot.high),sin(rshot.high))*20, 0.1, {1.0, 0.0, 0.0, 1.0});
		// draw_line(from, from + Vec(cos(rnew.low),  sin(rnew.low))*20,   0.07, {0.0, 0.0, 1.0, 1.0});
		// draw_line(from, from + Vec(cos(rnew.high), sin(rnew.high))*20,  0.07, {0.0, 0.0, 1.0, 1.0});

		return angle;
	}


	Object select_target(const World &world, int unit_idx, bool& target_is_fire_target)
	{
		const Unit &me =  world[unit_idx];
		const Unit *target_unit = get_fire_target(world, world[unit_idx]);
		target_is_fire_target = false;

		const Unit *buddy = 0;
		for(auto &u : world.units) if(u.is_teammate && u.id != me.id)
			buddy = &u;

		if(me.health < 61)
		{
			const Object *heal = get_closest_object<Object>(world.heals, world, me, true, [&](const Object &h) {
				return !buddy || !is_near(targets[*buddy], h);
			});
			if(heal)
				return *heal;
		}
		

		if(me.weapon.weapon_type == weapon_priority[0] || me.weapon.weapon_type == weapon_priority[1])
		{
			target_is_fire_target = true;
			return *target_unit;
		}

		const Weapon *res = 0;
		real best_score = -999999.0;
		for(const Weapon &w : world.weapons)
		{
			if(buddy && is_near(targets[*buddy], w))
				continue;

			PathFinder pf(world, Object::get_cell_of(w.x), Object::get_cell_of(w.y));

			real denemy = 999999.0;
			for(auto &enemy : world.units) if(!enemy.is_teammate)
				denemy = std::min(denemy, pf.get_dist_from(enemy));

			real dme = pf.get_dist_from(me);

			if(me.has_weapon() && (w.weapon_type != weapon_priority[0] || denemy < dme))
				continue;
			
			real score = 1000;
			if(w.weapon_type == weapon_priority[0])
				score = 3000;
			else if(w.weapon_type == weapon_priority[1])
				score = 2000;

			if(dme < denemy)
			{
				score += 100 - dme;
			}
			else
			{
				score = -12000 - dme;//+ denemy / (dme + EPS);
			}

			if(best_score < score)
			{
				best_score = score;
				res = &w;
			}
		}
		if(res)
			return *res;

		target_is_fire_target = true;
		return *target_unit;
	}


	bool do_suicide(const World &world, const Unit &me, Action &action)
	{
		if(!me.on_ground || !me.has_weapon() || 1 < me.weapon.fire_tick_left || world.is_on_ladder(me)
			 || world.cells[me.get_corner_celly(-1)-1][me.get_corner_cellx(0)] == ObjectType::NONE
			 || world.cells[me.get_corner_celly(-1)-1][me.get_corner_cellx(0)] == ObjectType::LADDER
		)
			return false;

		Pos mine_pos = me - Vec(0, me.size.y/2 - 0.25);
	
		int nmine = me.nmine, nplaced_mine = 0;
		for(auto mine : world.mines)
		{
			if(is_near(mine, mine_pos))
		
			{
				++nmine;
				++nplaced_mine;
			}
		}

		if(nmine <= 0)
			return false;

		if(me.weapon.weapon_type == WeaponType::BAZUKA)
			nmine += 1;

		int max_enemy_hp = 0, sum_enemy_hp = 0;
		int nenemy = 0, nmy = 0;
		for(const Unit &enemy : world.units)
		{
			if(is_intersect(enemy, mine_pos, 2.55 + nplaced_mine*0.2 - EPS, 2.55 + nplaced_mine*0.2 - EPS))
			{
				if(enemy.is_teammate != me.is_teammate)
				{
					max_enemy_hp = std::max(max_enemy_hp, enemy.health);
					sum_enemy_hp += enemy.health;
					nenemy += 1;
				}
				else
				{
					nmy += 1;	
				}
			}
		}
		if(my_team_size + nenemy < world.units.size() && (1 < nmy || me.weapon.weapon_type == WeaponType::BAZUKA || me.health+49)/50 <= (max_enemy_hp+49)/50)
			return false;

		int score_diff = world.scores[1] - world.scores[0];
		if(!me.is_teammate)
			score_diff *= -1;

		if(nenemy <= 1 && sum_enemy_hp <= score_diff)
			return false;

		if(nenemy == 0) // no enemy in range
			return false;

		if(nmine*50 < max_enemy_hp) // not enough mine
			return false;

		action.aim = Vec(0,-1);
		action.jump = false;
		action.jump_down = false;
		action.vel = 0.0;

		if(nplaced_mine*50 < max_enemy_hp)
		{
			action.plant_mine = true;
			nplaced_mine += 1;
		}

		if(nplaced_mine*50 >= max_enemy_hp)
			action.shoot = true;
		return true;
	}


	UnitsData<Action> solve(const World &world)
	{
		FUNCTION_STAT

		agressive_mode = last_score_change + 500 < world.tick && world.scores[0] <= world.scores[1];
		if(scores[0] != world.scores[0])
			last_score_change = world.tick;
		scores[0] = world.scores[0];
		scores[1] = world.scores[1];

		if(world.tick == 0)
			best_plans = AllUnitActions(world);

		heal_paths.clear();
		for(int i=0; i<world.heals.size(); ++i)
			heal_paths.push_back(PathFinder(world, Unit::get_cell_of(world.heals[i].x), Unit::get_cell_of(world.heals[i].y)));

		my_team_size = 0;
		for(auto &unit : world.units) if(unit.is_teammate)
		{
			targets[unit] = Object(ObjectType::NONE, Pos(0,0), Vec(1,1));
			my_team_size += 1;
		}
		assert(my_team_size == 1 || my_team_size == 2);

		std::vector<int> my;
		for(auto &unit : world.units) if(unit.is_teammate)
		{
			Object target = select_target(world, unit.idx, is_fire_target[unit]);
			targets_pf[unit] = PathFinder (world, Object::get_cell_of(target.x), Object::get_cell_of(target.y));
			targets[unit] = target;
			fire_targets[unit] = *get_fire_target(world, unit);
			my.push_back(unit.idx);
		}
		if(my.size() == 2 && targets[world[my[0]]].type == targets[world[my[1]]].type && !is_fire_target[world[my[0]]])
		{
			Pos p1 = targets[world[my[0]]];
			Pos p2 = targets[world[my[1]]];
			PathFinder pf1(world, Unit::get_cell_of(world[my[0]].x), Unit::get_cell_of(world[my[0]].y));
			PathFinder pf2(world, Unit::get_cell_of(world[my[1]].x), Unit::get_cell_of(world[my[1]].y));

			if(pf1.get_dist_from(p2) + pf2.get_dist_from(p1) < pf1.get_dist_from(p1) + pf2.get_dist_from(p2))
			{
				std::swap(targets[world[my[0]]], targets[world[my[1]]]);
			}
		}

		sim_res = SimulationResult();

		// PathFinder pf(world, Object::get_cell_of(world[0].x), Object::get_cell_of(world[0].y));
		// real w = 0.1;
		// for(real y=0.0; y<28.0; y+=w)
		// 	for(real x=0.0; x<38.0; x+=w)
		// 	{
		// 		real dist = pf.get_dist_from(Pos(x,y));
		// 		if(9999 < dist)
		// 			continue;
		// 		draw_rect(Pos(x,y), Vec(w,w), {0, 0, 1.0, 1.0 - dist/40.0});
		// 	}

		UnitsData<Action> res;
		for(const Unit &me : world.units) if(me.is_teammate)
		{
			go_to(world, me);		

			Action &action = sim_res[me].action0;

			if(do_suicide(world, me, action))
			{
				std::cerr << "#" << world.tick << "(" << time_spent << ") AA\n";
				res[me] = action;
				continue;
			}

			if(me.weapon.weapon_type != weapon_priority[0])// && (SQR(14) < me.dist2(*target_unit) || 30 < me.weapon.fire_tick_left))
			{
				for(const Weapon &weapon : world.weapons) if(is_intersect(me, weapon) && is_near(targets[me], weapon))
				{
					if(weapon.weapon_type == weapon_priority[0])
					{
						//dprint("swap_weapn");
						action.swap_weapon = true;
					}
				}
			}

			Vec target_dir = (fire_targets[me] - me).norm();
			bool shoot_now =
				me.weapon.weapon_type != WeaponType::NONE &&
				me.weapon.fire_tick_left <= 0 &&
				me.dist2(fire_targets[me]) < SQR(min_fire_dist) &&
				me.dist2(fire_targets[me]) < me.dist2(get_first_intersect_wall(world, me, target_dir, Vec(0.2,0.2)));

			real shoot_angle = calc_optimal_shoot_angle(world, me, fire_targets[me]);

			if(me.weapon.weapon_type == WeaponType::BAZUKA)
			{
				action.shoot = (8.0 < shoot_score);
			}
			else if(me.weapon.weapon_type == WeaponType::PISTOL)
			{
				action.shoot = (6.0 < shoot_score);
			}
			else if(sim_res[me].shoot_tick == world.tick && shoot_now && EPS < shoot_score)
			{
				action.shoot = true;
			}


			if(me.has_weapon() && !action.shoot)
			{
				real rot = fabs(angle_diff(me.weapon.last_angle, shoot_angle));
				dprint(me.weapon.spread, rot);
				if(me.weapon_params().max_spread < me.weapon.spread + rot) {
				}
				else if(me.weapon.spread - me.weapon_params().aim_speed/60.0 < me.weapon_params().min_spread) {
					real max_rot = me.weapon_params().min_spread - (me.weapon.spread - me.weapon_params().aim_speed/60.0);
					real rot = angle_diff(shoot_angle, me.weapon.last_angle);
					max_rot = std::min(max_rot, fabs(rot));
					shoot_angle = me.weapon.last_angle + sign(rot) * max_rot;
				}
				else {
					shoot_angle = me.weapon.last_angle;
					dprint("keep")
				}
			}

			// real rot = fabs(angle_diff(me.weapon.last_angle, shoot_angle));
			// real next_spread = std::min(me.weapon_params().max_spread, me.weapon.spread + rot);
			// next_spread -= me.weapon_params().aim_speed/60.0;
			// next_spread = std::max(me.weapon_params().min_spread, sim_res[me].next_spread);
			// dprint(me.weapon.spread, next_spread);

			action.aim = Vec(cos(shoot_angle), sin(shoot_angle));

			if(me.weapon.magazine < me.weapon_params().magazine_size && SQR(14) < me.dist2(fire_targets[me]))
			{
				action.reload = true;
			}

			res[me] = action;
		}

		return res;
	}
};


StarterStrategy starter;

void Solver::solve(const World &world)
{
	actions.clear();

	UnitsData<Action> res = starter.solve(world);
	for(int idx=0; idx<world.units.size(); ++idx) if(world[idx].is_teammate)
	{
		actions[world[idx].id] = res[world[idx]];
		actions[world[idx].id].jump_down &= world[idx].on_ground;
	}
}

} // strategy
