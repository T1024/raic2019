#include <queue>
#include <vector>
#include <algorithm>
#include "model/CustomData.hpp"
#include "Debug.hpp"
#include "world.h"
#include "utils.h"

namespace strategy
{

void draw_rect(Vec p, Vec size, const std::array<real,4> &color)
{
#ifdef _VIS
	gdebug->draw(CustomData::Rect(Vec2Float(float(p.x),float(p.y)), Vec2Float(size.x,size.y), ColorFloat(color[0],color[1],color[2],color[3])));
#endif
}

void draw_line(Vec p1, Vec p2, real width, const std::array<real,4> &color)
{
#ifdef _VIS
	gdebug->draw(CustomData::Line(Vec2Float(float(p1.x),float(p1.y)), Vec2Float(float(p2.x),float(p2.y)), float(width), ColorFloat(color[0],color[1],color[2],color[3])));
#endif
}

void draw_text(Vec p, const std::string &text, int aligment, real size, const std::array<real,4> &color)
{
#ifdef _VIS
	gdebug->draw(CustomData::PlacedText(text, Vec2Float(float(p.x),float(p.y)), (TextAlignment)aligment, float(size), ColorFloat(color[0],color[1],color[2],color[3])));
#endif
}


bool is_intersect(const Object &o1, const Object &o2, real bound)
{
	return (
		fabs(o1.x - o2.x) < o1.size.x/2 + o2.size.x/2 + bound &&
		fabs(o1.y - o2.y) < o1.size.y/2 + o2.size.y/2 + bound
	);
}


bool is_intersect(const Object &o1, const Pos &o2, real radiusx, real radiusy)
{
	return (
		fabs(o1.x - o2.x) < o1.size.x/2 + radiusx &&
		fabs(o1.y - o2.y) < o1.size.y/2 + radiusy
	);
}


bool is_line_intersect(const Pos &p11, const Pos &p12, const Pos &p21, const Pos &p22)
{
	real z11 = (p12 - p11) % (p21 - p11);
	real z12 = (p12 - p11) % (p22 - p11);
	if(0.0 < z11 * z12)
		return false;

	real z21 = (p22 - p21) % (p11 - p21);
	real z22 = (p22 - p21) % (p12 - p21);
	if(0.0 < z21 * z22)
		return false;

	return true;
}


Pos get_first_intersect_wall(const World &world, const Pos &p0, const Vec2D &dir, const Vec2D &size)
{
	FUNCTION_STAT
	Vec px = p0 + dir*100;
	real mind = 999999;
	Vec bestp(-1,-1);

	for(auto &wall : world.walls)
	{
		Vec s = wall.size + size;
		Pos p1 = wall + Vec(-s.x/2, -s.y/2);
		Pos p2 = wall + Vec( s.x/2, -s.y/2);
		Pos p3 = wall + Vec( s.x/2,  s.y/2);
		Pos p4 = wall + Vec(-s.x/2,  s.y/2);

		if(
			is_line_intersect(p1, p2, p0, px) ||
			is_line_intersect(p2, p3, p0, px) ||
			is_line_intersect(p3, p4, p0, px) ||
			is_line_intersect(p4, p1, p0, px)
		) {
			if(p0.dist2(wall) < mind)
			{
				mind = p0.dist2(wall);
				bestp = wall;
			}
		}
	}

	assert(0.0 < bestp.x);
	return bestp;
}


Range get_view_range(const Pos &me, const Object &he, const Vec &dir0)
{
	Range res;

	Vec corners[4] = {he+he.size*0.5, he + Vec(-he.size.x/2,he.size.y/2), he-he.size*0.5, he + Vec(he.size.x/2,-he.size.y/2)};
	for(int i=0; i<4; ++i)
	{
		Vec dir = (corners[i] - me).norm();
		real angle = atan2(dir0 % dir, dir0 * dir);
		res.expand(angle);
	}

	return res;
}


real angle_diff(real a, real b)
{
	real diff = a-b;
	if(diff < -PI)
		diff += PI;
	if(PI < diff)
		diff -= PI;

	assert(-PI <= diff && diff <= PI);
	return diff;
}


PathFinder::PathFinder(const World &world, int xo, int yo)
{
	FUNCTION_STAT
	for(int y=0; y<path_cell.size_y(); ++y)
		for(int x=0; x<path_cell.size_x(); ++x)
			path_cell[y][x] = PathCell();

	std::queue<Vec2I> q;
	path_cell[yo][xo].cost = 0;
	q.push(Vec2I(xo,yo));

	while(!q.empty())
	{
		Vec2I p = q.front();
		q.pop();

		for(int dy=-1; dy<=1; ++dy)
			for(int dx=-1; dx<=1; ++dx) if(!dx || !dy)
			{
				Vec2I p1(p.x+dx, p.y+dy);
				if(world.cells[p1.y][p1.x] == ObjectType::WALL)
					continue;
				if(path_cell[p].cost + 1 < path_cell[p1].cost)
				{
					path_cell[p1].cost = path_cell[p].cost + 1;
					path_cell[p1].from = p;
					q.push(p1);
				}
			}
	}
}


bool PathFinder::build_path(Vec2I p1, Vec2I p2, std::vector<Vec2I> &path) const
{
	path.clear();

	assert(path_cell[p1].cost == 0);
	if(999999 <= path_cell[p2].cost) {
		assert(0);
		return false;
	}

	Vec2I p = p2;
	while(p != p1 && path.size() < 99)
	{
		path.push_back(p);

		assert(path_cell[p].cost < 9999);
		p = path_cell[p].from;
	}

	std::reverse(path.begin(), path.end());
	return path.size() < 99;
}


real PathFinder::interpolate(real c1, real c2, real q)
{
	//assert(c1 < 9999 || c2 < 9999);
	//dprint(c1, c2);
	if(c1 > 9999)
		c1 = c2+1;
	if(c2 > 9999)
		c2 = c1+1;
	assert(std::abs(c1-c2) < 3+EPS);

	real c = c1*q + c2*(1.0-q);
	return c;
}


real PathFinder::get_dist_from(const Pos &pos) const
{
	//return path_cell[Vec2I(Unit::get_cell_of(pos.x), Unit::get_cell_of(pos.y))].cost;
	//assert(path_cell[Vec2I(Unit::get_cell_of(pos.x), Unit::get_cell_of(pos.y))].cost < 9999);
	if((path_cell[Vec2I(Unit::get_cell_of(pos.x), Unit::get_cell_of(pos.y))].cost > 9999))
		return 999999;

	int x1 = (int)floor(pos.x - 0.5);
	int y1 = (int)floor(pos.y - 0.5);
	real qx = (1.0 - (pos.x-0.5-x1));
	real qy = (1.0 - (pos.y-0.5-y1));
	assert(0.0 <= qx <= 1.0 && 0.0 <= qy <= 1.0);

	real d1 = interpolate(path_cell[y1][x1].cost, path_cell[y1][x1+1].cost, qx);
	real d2 = interpolate(path_cell[y1+1][x1].cost, path_cell[y1+1][x1+1].cost, qx);
	real d = interpolate(d1, d2, qy);
	return d;
}

} // namespace strategy
