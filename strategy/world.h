#ifndef _WORLD_H_
#define _WORLD_H_

#include <vector>
#include <array>
#include "object.h"
#include "action.h"


extern double time_spent;
extern double time_left;
extern bool fast_mode;

namespace strategy
{
	struct AllUnitActions;

	struct World
	{
		// Constants
		static int X, Y;
		static WeaponParams weapon_params[4];
		static Table2T<ObjectType, 64, 32> cells;
		static std::vector<Object> walls;
		static int unit_max_health;
		static real unit_max_speed;


		std::vector<Unit> units;
		std::vector<Bullet> bullets;
		std::vector<Object> heals;
		std::vector<Weapon> weapons;
		std::vector<Mine> mines;
		std::vector<Object> loot_mines;
		int tick = -1;

		int scores[2] = {0,0};

		const Unit &operator[] (int i) const { assert(i < units.size()); return units[i]; }
		Unit &operator[] (int i) { assert(i < units.size()); return units[i]; }

		bool is_on_ground(const strategy::Unit &unit, int diry) const;
		bool is_on_ladder(const strategy::Unit &unit) const;

		void apply_action_to_unit(Unit &unit, const Action &action) const;

		void simulate_tick(AllUnitActions &actions);

		// TODO: private
		void check_state() const;
		Vec2I get_coord_order(const Pos &p1, const Pos &p2, const Vec2I &ic0, const Vec2I &ic) const;
		Vec2I get_coord_order(const Unit &unit, const Vec &vel) const;
		bool move_unit(Unit &unit, int coord_id, const Action &act, int sub_tick = 1) const;
		void explode(const Pos &p0, real radius, int damage, const std::vector<Unit> &units0, real subtick);
		void do_mines(Unit &unit);
    };
};

#endif
