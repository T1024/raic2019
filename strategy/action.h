#ifndef _ACTION_H_
#define _ACTION_H_

namespace strategy {
    struct Action
    {
        Vec2D aim = Vec2D(0,0);
        double vel = 0.0;
        bool jump = false;
        bool jump_down = false;
        bool shoot = false;
        bool swap_weapon = false;
        bool reload = false;
        bool plant_mine = false;
    };
}

#endif
