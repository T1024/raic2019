#ifndef _POS_H_
#define _POS_H_

#include <assert.h>
#include <cmath>
#include <iostream>

template<class T>
struct Pos2T
{
	Pos2T() {}
	Pos2T(T x, T y)
		: x(x)
		, y(y)
	{}

	bool operator== (const Pos2T &b) const
	{
		return x == b.x && y == b.y;
	}

	bool operator!= (const Pos2T &b) const
	{
		return x != b.x || y != b.y;
	}

	Pos2T operator*(T q) const
	{
		return Pos2T(x*q,y*q);
	}
	
	Pos2T operator+(const Pos2T &b) const
	{
		return Pos2T(x+b.x,y+b.y);
	}
	
	Pos2T &operator+=(const Pos2T &b)
	{
		x += b.x;
		y += b.y;
		return *this;
	}
	
	Pos2T operator-(const Pos2T &b) const
	{
		return Pos2T(x-b.x,y-b.y);
	}
	
	Pos2T &operator-=(const Pos2T &b)
	{
		x -= b.x;
		y -= b.y;
		return *this;
	}
	
	Pos2T operator-() const
	{
		return Pos2T(-x,-y);
	}
	
	Pos2T &operator*=(T q)
	{
		x *= q;
		y *= q;
		return *this;
	}
	
	T operator*(const Pos2T &b) const
	{
		return x*b.x + y*b.y;
	}
	
	T operator%(const Pos2T &b) const
	{
		return x*b.y - y*b.x;
	}

	const Pos2T &pos() const
	{
		return *this;
	}
	
	double len() const 
	{
		return sqrt(x*x+y*y);
	}
	
	T len2() const 
	{
		return x*x+y*y;
	}
	
	Pos2T norm() const
	{
		return (*this)*(1.0/len());
	}

	Pos2T rot90() const
	{
		return Pos2T(-y, x);
	}
	
	double dist(const Pos2T &p) const
	{
		return sqrt((x-p.x)*(x-p.x) + (y-p.y)*(y-p.y));
	}
	
	T dist2(const Pos2T &p) const
	{
		return (x-p.x)*(x-p.x) + (y-p.y)*(y-p.y);
	}	
	
	bool operator<(const Pos2T &b) const
	{
		if(y != b.y)
			return y < b.y;
		return x < b.x;
	}

	T &operator[] (int i)
	{
		assert(i == 0 || i == 1);
		return (i == 0 ? x : y);
	}

	const T &operator[] (int i) const
	{
		assert(i == 0 || i == 1);
		return (i == 0 ? x : y);
	}

	T x, y;
};


template<class T, int X, int Y>
struct Table2T
{
	typedef T T1[X];
	T a[Y][X];

	Table2T()
	{
		//memset(a, 0, sizeof(a));
	}
	
	int size_y() const
	{
		return Y;
	}

	int size_x() const
	{
		return X;
	}

	T1 &operator[] (int y)
	{
		assert(0 <= y && y < Y);
		return a[y];
	}

	const T1 &operator[] (int y) const
	{
		assert(0 <= y && y < Y);
		return a[y];
	}

	T &operator[] (const Pos2T<int> &p)
	{
		assert(0 <= p.y && p.y < Y);
		assert(0 <= p.x && p.x < X);
		return a[p.y][p.x];
	}

	const T &operator[] (const Pos2T<int> &p) const
	{
		assert(0 <= p.y && p.y < Y);
		assert(0 <= p.x && p.x < X);
		return a[p.y][p.x];
	}
};


template<class T>
std::ostream& operator<<(std::ostream &s,const Pos2T<T> &p)
{
	s << "(" << p.x << "," << p.y << ")";
	return s;
}

#endif // _POS_H_
