#ifndef _ACTION_PLAN_H_
#define _ACTION_PLAN_H_

#include <vector>
#include "world.h"
#include "action.h"

namespace strategy {

struct TimedAction : public Action
{
	TimedAction() {}
	
	TimedAction(const Action &act, int tick)
		: Action(act)
		, tick(tick)
	{}
	
	int tick;
};


struct ActionSequence
{
	int unit_id = -1;
	std::vector<TimedAction> actions;
	int tick, tick0;
	int iter_idx = 0;
	
	ActionSequence() {}
	
	ActionSequence(const World &world, int id)
	{
		tick = tick0 = world.tick;
		unit_id = id;
	}
	
	void reset(const World &world)
	{
		//assert(unit_id != -1);
		actions.clear();
		tick = tick0 = world.tick;
		iter_idx = 0;
	}
	
	void add(const TimedAction &action)
	{
		//assert(!actions.empty() || tick0 == action.tick);
		assert(actions.empty() || actions.back().tick < action.tick);
		actions.push_back(action);
	}
	
	Action get(int tick) const
	{
		int idx = 0;
		while(idx < actions.size() && actions[idx].tick < tick)
			++idx;
		if(idx < actions.size())
			return actions[idx];
		//assert(0);
		return Action();
	}

	void start_iter()
	{
		tick = tick0;
		iter_idx = 0;
	}
	
	Action get_actual(int tick)
	{
		assert(iter_idx == 0 || actions[iter_idx-1].tick < tick);
		while(iter_idx < actions.size() && actions[iter_idx].tick < tick)
			++iter_idx;
		if(iter_idx < actions.size())
			return actions[iter_idx];
		//assert(0);
		return Action();
	}
};


struct AllUnitActions
{
	std::vector<ActionSequence> ra;
	
	AllUnitActions() {}
	
	AllUnitActions(const World &world)
	{
		int max_id = 0;
		for(const Unit &unit : world.units)
			max_id = std::max(max_id, unit.id);
		ra.resize(max_id+1);

		for(auto &unit : world.units)
			set(unit.id, ActionSequence(world, unit.id));
	}
	
	void set(int id, const ActionSequence &as)
	{
		assert(0 <= id && id < ra.size());
		ra[id] = as;
	}
	
	ActionSequence &operator() (int id)
	{
		assert(0 <= id && id < ra.size());
		return ra[id];
	}
	
	const ActionSequence &operator() (int id) const
	{
		assert(0 <= id && id < ra.size());
		return ra[id];
	}
};


} // namespace strategy

#endif // _ACTION_PLAN_H_
