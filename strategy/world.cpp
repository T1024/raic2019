#include <assert.h>
#include "world.h"
#include "utils.h"
#include "action_plan.h"

namespace strategy {

const WeaponParams &Unit::weapon_params() const
{
	return World::weapon_params[(int)weapon.weapon_type];
}


WeaponParams World::weapon_params[4];

int World::X = -1;
int World::Y = -1;
Table2T<ObjectType, 64, 32> World::cells;
std::vector<Object> World::walls;
int World::unit_max_health = -1;
real World::unit_max_speed = -1;


bool World::is_on_ladder(const strategy::Unit &unit) const
{
	int iy1 = unit.get_corner_celly(-1);
	int iy2 = unit.get_corner_celly(1);
	int ix = unit.get_center_cell_x();
	if(cells[iy1][ix] == strategy::ObjectType::LADDER || cells[iy2][ix] == strategy::ObjectType::LADDER)
		return true;
	return false;
}

bool World::is_on_ground(const strategy::Unit &unit, int diry) const
{
	int iy = unit.get_bottom_cell_y();
	int iy2 = unit.get_corner_celly(1);
	int ix = unit.get_center_cell_x();
	if(cells[iy][ix] == strategy::ObjectType::LADDER || cells[iy2][ix] == strategy::ObjectType::LADDER)
		return true;

	if(diry > 0)
		return false;
	
	if(EPS < fabs(unit.y-unit.size.y/2.0 - iy))
		return false;

	for(int x=unit.get_corner_cellx(-1); x<=unit.get_corner_cellx(1); ++x)
		if(cells[iy-1][x] == strategy::ObjectType::WALL || cells[iy-1][x] == strategy::ObjectType::PLATFORM || (cells[iy-1][x] == strategy::ObjectType::LADDER && cells[iy][x] == strategy::ObjectType::NONE))
			return true;

	return false;
}

void World::check_state() const
{
	FUNCTION_STAT

	int idx =0;
	for(const Unit &unit : units)
	{
		assert(idx == unit.idx);
		int iy1 = unit.get_corner_celly(-1);
		int iy2 = unit.get_corner_celly(1);
		int ix1 = unit.get_corner_cellx(-1);
		int ix2 = unit.get_corner_cellx(1);

		assert(ix2-ix1 <= 2);
		assert(iy2-iy1 <= 3);
		for(int iy=iy1; iy<=iy2; ++iy)
			for(int ix=ix1; ix<=ix2; ++ix)
			{
				assert(cells[iy][ix] != ObjectType::WALL);
			}

		for(const Unit &unit2 : units) if(unit.idx < unit2.idx)
		{
			bool intersect = is_intersect(unit, unit2, -EPS);
			assert(!intersect);
		}

		++idx;
	}
}

bool World::move_unit(Unit &unit, int coord_id, const Action &act, int nsub_tick) const
{
	if(fabs(unit.vel[coord_id]) < EPS)
		return false;

	FUNCTION_STAT
	Pos unit0 = unit;
	int move_sign = sign(unit.vel[coord_id]);

	int ia_prev  = unit.get_corner_cell_coord(coord_id, move_sign);

	unit[coord_id] += unit.vel[coord_id] / (60.0*nsub_tick);

	int ia  = unit.get_corner_cell_coord(coord_id, move_sign);
	int ib1 = unit.get_corner_cell_coord(1-coord_id, -1);
	int ib2 = unit.get_corner_cell_coord(1-coord_id,  1);

	bool on_jump_pad = false;
	real limit = 9999;
	if(ia_prev != ia)
	{
		for(int ib=ib1; ib<=ib2; ++ib)
		{
			Vec2I ip;
			ip[coord_id] = ia;
			ip[1-coord_id] = ib;

			if(cells[ip] == ObjectType::JUMP_PAD)
				on_jump_pad = true;

			bool collision = (cells[ip] == ObjectType::WALL);
			collision |= (coord_id == 1 && move_sign < 0 && cells[ip] == ObjectType::PLATFORM && ia != ia_prev && !act.jump_down);

			if(collision)
			{
				real r = ia + (1 - move_sign)/2;
				limit = std::min(limit, r*move_sign);
				break;
			}
		}

		if(on_jump_pad && 999 < limit)
		{
			unit.jump_tick_left = 33;
			unit.jump_speed = 20.0;
			unit.can_stop_jump = false;
		}
	}

	bool from_unit = false; // TODO: kell?
	for(int i=0; i<units.size(); ++i) if(i != unit.idx)
		if(is_intersect(unit, units[i], -EPS) && unit[coord_id] - unit.size[coord_id]/2 < units[i][coord_id] + units[i].size[coord_id]/2)
		{
			real r = units[i][coord_id] - move_sign * units[i].size[coord_id]/2.0;
			if(r*move_sign < limit)
			{
				from_unit = true;
			}
			limit = std::min(limit, r*move_sign);
		}

	limit *= move_sign;

	if(fabs(limit) < 999)
	{
		real prev = unit[coord_id];
		unit[coord_id] = limit - move_sign*unit.size[coord_id]/2.0;
		if(coord_id == 1 && (act.jump || 0 < move_sign))
		{
			// if(0 < move_sign) // TODO: kell?
			// 	unit.jump_tick_left = 0;
			// else
			// 	unit.jump_tick_left = 33;

			if(!from_unit)
			{
				unit[coord_id] += (unit[coord_id] - prev);
				if(move_sign < 0)
					unit[coord_id] = std::min(unit0[coord_id], unit[coord_id]);
				else
					unit[coord_id] = std::max(unit0[coord_id], unit[coord_id]);
			}
		}
		return true;
	}

	return false;
}


Unit get_unit_at_subtick(const Unit &unit0, const Unit &unit1, real subtick)
{
	Unit u = unit0;
	u += unit1.vel * (subtick/60.0);
	for(int ic=0; ic<2; ++ic)
	{
		if(fabs(unit1[ic] - unit0[ic]) < fabs(u[ic] - unit0[ic]))
			u[ic] = unit1[ic];
	}
	return u;
}

void World::explode(const Pos &p0, real radius, int damage, const std::vector<Unit> &units0, real subtick)
{
	for(Unit &unit : units)
	{
		Unit u = get_unit_at_subtick(units0[unit.idx], unit, subtick);
		if(is_intersect(u, p0, radius, radius))
		{
			unit.health = std::max(0, unit.health - damage);
		}
	}
}


void World::do_mines(Unit &unit)
{
	for(Mine &mine : mines) if(mine.state == MineState::IDLE)
	{
		if(is_intersect(unit, mine, EPS))
		{
			//explode(mine, mine.exp_radius, mine.damage);
			mine.state = MineState::TRIGGERED;
			mine.timer = 30;
		}
	}
}


void World::apply_action_to_unit(Unit &unit, const Action &act) const // TODO: code duplicate
{
	unit.can_stop_jump = !(15.0 < unit.jump_speed && 0 < unit.jump_tick_left);
	bool jump = !unit.can_stop_jump || (act.jump && 0 < unit.jump_tick_left);
	bool on_ground = is_on_ground(unit, -1); // TODO
	bool jump_down = /*!on_ground && */unit.jump_tick_left <= 0;

	//if(!unit.is_teammate && !(!unit.can_stop_jump || jump_down))
	//	return;

	unit.vel = Vec(0,0);

	if(jump) {
		unit.vel.y = unit.jump_speed;//unit_max_speed;
	}
	else if(!on_ground || act.jump_down) {
		unit.vel.y = -unit_max_speed;
	}

	unit.vel.x = clamp(act.vel, -unit_max_speed, unit_max_speed);
}


bool dbg1 = false;

Vec2I World::get_coord_order(const Pos &p1, const Pos &p2, const Vec2I &ic0, const Vec2I &ic) const
{
	Vec2I ip1((int)p1.x, (int)p1.y);
	Vec2I ip2((int)p2.x, (int)p2.y);
	if(ip1.x == ip2.x || ip1.y == ip2.y)
		return Vec2I(0,0);

	if(dbg1)
		draw_rect(Pos(ic0.x,ic0.y), Vec(1,1), {0,1,1,0.5});

	Vec2I idir(sign(p2.x-p1.x), sign(p2.y-p1.y));

	Vec c(ic.x, ic.y);
	if(dbg1)
	{
		//draw_rect(c - Vec(0.1,0.1), Vec(0.2,0.2), {1,1,0,1});
		draw_rect(p1 - Vec(0.1,0.1), Vec(0.1,0.1), {1,1,0,1});
		draw_rect(p2 - Vec(0.1,0.1), Vec(0.1,0.1), {1,1,0,1});
	}

	Vec2I res;
	if(sign((p2-p1) % (c-p1)) == sign((p2-p1) % Vec(idir.x,0)))
		res = Vec2I(0,1);
	else
		res = Vec2I(1,0);

	if(dbg1)
		draw_line(p1, p1 + Vec(res.x,res.y), 0.1, {1,1,0,1});

	return res;
}


Vec2I World::get_coord_order(const Unit &unit, const Vec &vel) const
{
	if(fabs(vel.x) < EPS || fabs(vel.y) <EPS)
		return Vec2I(0,0);

	Vec2I res(0,0);

	Vec dr = vel * (1.0/60.0);
	Vec2I idir(sign(vel.x), sign(vel.y));

	{
		Vec p1 = unit + Vec(unit.size.x/2*idir.x, unit.size.y/2*idir.y);
		Vec p2 = p1 + dr;

		Vec2I ic((int)p2.x, (int)p2.y);
		if(cells[ic] == ObjectType::WALL || cells[ic] == ObjectType::JUMP_PAD)
			res += get_coord_order(p1, p2, ic, Vec2I(ic.x + (idir.x < 0 ? 1 : 0), ic.y + (idir.y < 0 ? 1 : 0)));
	}

	{
		Vec p1 = unit + Vec(-unit.size.x/2*idir.x, unit.size.y/2*idir.y);
		Vec p2 = p1 + dr;

		Vec2I ic((int)p1.x, (int)p2.y);
		if((0 < idir.y && cells[ic] == ObjectType::WALL) || cells[ic] == ObjectType::JUMP_PAD)
			res += get_coord_order(p1, p2, ic, Vec2I(ic.x + (idir.x > 0 ? 1 : 0), ic.y + (idir.y < 0 ? 1 : 0)));
	}

	{
		Vec p1 = unit + Vec(unit.size.x/2*idir.x, -unit.size.y/2*idir.y);
		Vec p2 = p1 + dr;

		Vec2I ic((int)p2.x, (int)p1.y);
		if(cells[ic] == ObjectType::JUMP_PAD)
			res += get_coord_order(p1, p2, ic, Vec2I(ic.x + (idir.x < 0 ? 1 : 0), ic.y + (idir.y > 0 ? 1 : 0)));
	}

	return res;
}

void World::simulate_tick(AllUnitActions &actions)
{
	FUNCTION_STAT
	//check_state();

	auto units0 = units;

	for(Unit &unit : units)
	{
		if(unit.health <= 0)
			continue;

		const Action &act0 = actions(unit.id).get_actual(tick);
		Unit unit0 = unit;

		Action act;
		//if(unit.is_teammate)
		{
			act = act0;
		}
		// else
		// {
		// 	act.jump = false;//unit.jump_tick_left < 33;
		// 	act.jump_down = false;
		// }

		unit.weapon.fire_tick_left -= 1;

		unit.can_stop_jump = !(15.0 < unit.jump_speed && 0 < unit.jump_tick_left);
		bool jump = !unit.can_stop_jump || (act.jump && 0 < unit.jump_tick_left);
		bool on_ground = unit.on_ground;
		bool jump_down = /*!on_ground && */unit.jump_tick_left <= 0;

		if(!unit.is_teammate && !(!unit.can_stop_jump || jump_down) && 50 < unit.health)
			continue;

		unit.vel = Vec(0,0);

		int diry = 0;
		if(jump) {
			unit.vel.y = unit.jump_speed;//unit_max_speed;
			diry = 1;
		}
		else if(!on_ground || act.jump_down) {
			unit.vel.y = -unit_max_speed;
			diry = -1;
		}

		unit.vel.x = clamp(act.vel, -unit_max_speed, unit_max_speed);

		Vec2I order = get_coord_order(unit, unit.vel);

		bool collision_y;
		bool collision_x;
		if(!order.x || order.y)
		{
			collision_y = move_unit(unit, 1, act);
			collision_x = move_unit(unit, 0, act);
		}
		else
		{
			collision_x = move_unit(unit, 0, act);			
			collision_y = move_unit(unit, 1, act);
		}

		jump = !unit.can_stop_jump || (act.jump && 0 < unit.jump_tick_left);

		int iy1 = unit.get_corner_celly(-1);
		int ix1 = unit.get_corner_cellx(-1);
		int ix2 = unit.get_corner_cellx(1);
		unit.on_ground = is_on_ground(unit, diry);

		if(diry == 1 && collision_y)
		{
			unit.jump_tick_left = 0;
		}
		else if(cells[iy1][ix1] == ObjectType::JUMP_PAD || cells[iy1][ix2] == ObjectType::JUMP_PAD)
		{
			unit.jump_tick_left = 33;
			unit.jump_speed = 20.0;
		}
		else if(unit.on_ground)
		{
			unit.jump_tick_left = 33;
			unit.jump_speed = 10.0;
		}
		else if(jump)
			unit.jump_tick_left -= 1;
		else
			unit.jump_tick_left = 0;

		do_mines(unit);
	}


	for(Unit &unit : units)
	{
		if(unit.health <= 0)
			continue;

		const Unit &unit0 = units0[unit.idx];

		int vel_sign[2] = {sign(unit.vel.x), sign(unit.vel.y)};
		for(int i=0; i<bullets.size(); ++i) if(bullets[i].unit_id != unit.id) // TODO
		{
			const Bullet &bullet = bullets[i];
			Object u = unit0;
			int nsubtick = 100; // TODO: speedup
			real bound = EPS;// + std::max(fabs(bullet.vel.x) + fabs(unit.vel.x), fabs(bullet.vel.y) + fabs(unit.vel.y)) / (60*nsubtick);
			Object b = bullet;
			bool intersected = false;
			for(int sub_tick=0; sub_tick<nsubtick; ++sub_tick)
			{
				b += bullets[i].vel * (1.0/(60*nsubtick));
				u += unit.vel * (1.0/(60*nsubtick));
				for(int ic=0; ic<2; ++ic)
					if(EPS < (u[ic] - unit[ic])*vel_sign[ic])
						u[ic] = unit[ic];

				if(is_intersect(b, u, bound))
				{
					assert(0 <= i && i < bullets.size());
					unit.health = std::max(0, unit.health - bullet.damage);
					if(bullets[i].exp_damage)
						explode(b, bullets[i].exp_size, bullets[i].exp_damage, units0, (sub_tick+1) *1.0 / nsubtick);
					bullets.erase(bullets.begin() + i);
					i--;
					intersected = true;
					break;
				}
			}

			if(!intersected)
			{
				assert(is_near(u, unit));
			}
		}

		// if(unit.health < 100)
		// for(int i=0; i<heals.size(); ++i) if(heals[i].valid)
		// {
		// 	if(is_intersect(unit, heals[i], EPS))
		// 	{
		// 		unit.health = std::min(100, unit.health + 50);
		// 		heals[i].valid = false;
		// 	}
		// }

		// for(int i=0; i<loot_mines.size(); ++i) if(loot_mines[i].valid)
		// {
		// 	if(is_intersect(unit, loot_mines[i], EPS))
		// 	{
		// 		unit.nmine += 1;
		// 		loot_mines[i].valid = false;
		// 	}
		// }
	}

	for(int i=0; i<bullets.size(); ++i)
	{
		if(bullets[i].last_tick <= tick)
		{
			assert(bullets[i].last_tick == tick);
			if(bullets[i].exp_damage) 
				explode(bullets[i].last_pos, bullets[i].exp_size, bullets[i].exp_damage, units0, bullets[i].last_subtick);
			bullets.erase(bullets.begin() + i);
			i--;
			continue;
		}
		bullets[i] += bullets[i].vel * (1.0/60.0);
	}

	for(Mine &mine : mines)
	{
		mine.timer -= 1;
		if(mine.timer <= 0)
		{
			mine.state = (MineState)((int)mine.state+1);
			mine.timer = 30;
			if(mine.state == MineState::EXPLODED)
			{
				explode(mine, mine.exp_radius, mine.damage, units0, 0);
			}
		}
	}

	tick += 1;

	//check_state();
}

} // strategy
