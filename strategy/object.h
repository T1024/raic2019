#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "base.h"

namespace strategy
{

enum class ObjectType
{
	NONE,
	WALL,
	PLATFORM,
	LADDER,
	JUMP_PAD,
	UNIT,
	BULLET,
	HEAL,
	WEAPON,
	MINE,
	OTHER
};


struct Object : public Vec2D // center
{
	Object() {}

	Object(ObjectType type, Vec2D pos, Vec2D size)
		: type(type)
		, Vec2D(pos)
		, size(size)
	{}

	int get_bottom_cell_y() const
	{
		return (int)(y - size.y/2.0 + EPS);
	}

	int get_center_cell_x() const
	{
		return (int)(x);
	}

	int get_corner_cellx(int dx) const
	{
		real cx = x + size.x*((1.0-EPS)/2.0)*dx;
		return (int)(cx);
	}

	int get_corner_celly(int dy) const
	{
		real cy = y + size.y*((1.0-EPS)/2.0)*dy;
		return (int)(cy);
	}

	int get_corner_cell_coord(int ic, int dc) const
	{
		real cooord = (*this)[ic] + size[ic]/2.0*dc*(1.0-EPS);
		return (int)(cooord);
	}

	static int get_cell_of(real xy)
	{
		return (int)(xy);
	}

	Vec2I get_cell() const
	{
		return Vec2I(get_cell_of(x), get_cell_of(y));
	}

	ObjectType type;
	Vec2D size;

	bool valid = true; // TODO: move
};


enum class WeaponType
{
	NONE,
	PISTOL,
	RIFLE,
	BAZUKA
};


struct WeaponParams
{
	int magazine_size = -1;
	int fire_delay = -1;
	real min_spread = -1.0;
	real max_spread = -1.0;
	real recoil = -1.0;
	real aim_speed = -1.0;
	real bullet_speed = -1.0;
	real bullet_size = -1.0;
	real damage = -1.0;
	int exp_damage = 0;
	real exp_size = 0.0;
};


struct Weapon : public Object
{
	WeaponType weapon_type;
	real spread = -1.0;
	int fire_tick_left = -1;
	real last_angle = 0.0;
	int magazine = -1;

	Weapon(Vec2D pos, Vec2D size, WeaponType weapon_type)
		: Object(ObjectType::WEAPON, pos, size)
		, weapon_type(weapon_type)
	{}
};


struct Unit : public Object
{
	Unit() {}

	Unit(Vec2D pos, Vec2D size, Vec2D vel, int jump_tick, bool can_stop_jump, real jump_speed, int id, int idx, bool teammate, int health, const Weapon &weapon)
		: Object(ObjectType::UNIT, pos, size)
		, vel(vel)
		, jump_tick_left(jump_tick)
		, can_stop_jump(can_stop_jump)
		, jump_speed(jump_speed)
		, id(id)
		, idx(idx)
		, is_teammate(teammate)
		, health(health)
		, weapon(weapon)
	{}

	bool has_weapon() const
	{
		return weapon.weapon_type != WeaponType::NONE;
	}

	const WeaponParams &weapon_params() const;

	Vec2D vel = Vec2D(0,0);

	int jump_tick_left = -1;
	bool can_stop_jump = false;
	real jump_speed = 10.0;
	bool on_ground = false;

	int id = -1;
	int idx = -1, local_idx = -1;
	bool is_teammate = false;

	int health = -1;
	int nmine = 0;
	Weapon weapon = Weapon(Vec(-1,-1), Vec(-1,-1), WeaponType::NONE);
};


struct Bullet : public Object
{
	Bullet(Vec pos, Vec size, Vec vel, WeaponType weapon_type, int damage, int unit_id)
		: Object(ObjectType::BULLET, pos, size)
		, vel(vel)
		, weapon_type(weapon_type)
		, damage(damage)
		, unit_id(unit_id)

	{}

	Vec2D vel = Vec2D(0,0);

	WeaponType weapon_type;
	int damage;
	int exp_damage = 0;
	real exp_size = 0.0;

	int unit_id = -1;

	int last_tick = -1;
	real last_subtick = 0.0;
	Pos last_pos;
};


enum class MineState {
    PREPARING = 0,
    IDLE = 1,
    TRIGGERED = 2,
    EXPLODED = 3
};


struct Mine : public Object
{
	Mine(Vec pos, Vec size, MineState state, int damage, real exp_radius, int timer)
		: Object(ObjectType::MINE, pos, size)
		, state(state)
		, damage(damage)
		, exp_radius(exp_radius)
		, timer(timer)
	{}

	MineState state;
	int damage;
	real exp_radius;
	int timer;
};


}; // strategy

#endif // _OBJECT_H_
