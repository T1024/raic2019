#include <assert.h>
#include <sys/time.h>
#include "MyStrategy.hpp"
#include "utils.h"
#include "world.h"
#include "solver.h"
#ifdef _TIMING
#	include <dbg_timing.cpp>
#endif

double get_time()
{
	timeval tv;
	gettimeofday(&tv,0);
	return tv.tv_sec + 1e-6*tv.tv_usec;
}

double time_spent = 0.0;
double time_left = 80.0;
#ifdef _FAST
	bool fast_mode = true;
#else
	bool fast_mode = false;
#endif

Debug *gdebug = 0;

int time_to_tick(double t)
{
	return round(t*60.0);
}

MyStrategy::MyStrategy() {}

strategy::WeaponType convert_weapon_type(WeaponType type)
{
	strategy::WeaponType wt;
	switch(type)
	{
		case PISTOL: wt = strategy::WeaponType::PISTOL; break;
		case ASSAULT_RIFLE: wt = strategy::WeaponType::RIFLE; break;
		case ROCKET_LAUNCHER: wt = strategy::WeaponType::BAZUKA; break;
		default: assert(0);
	}
	return wt;
}


void build_world(const Unit &my_unit, const Game &game, strategy::World &world)
{
	world = strategy::World();
	world.tick = game.currentTick;
	world.unit_max_health = game.properties.unitMaxHealth;
	world.unit_max_speed = game.properties.unitMaxHorizontalSpeed;

	real cell_size = 1.0; // ?
	world.walls.clear();

	world.X = game.level.tiles.size();
	world.Y = game.level.tiles[0].size();
	for(int x=0; x<game.level.tiles.size(); ++x)
	{
		for(int y=0; y<game.level.tiles[x].size(); ++y)
		{
			const auto &t = game.level.tiles[x][y];
			if(t == WALL)
				world.walls.push_back(strategy::Object(strategy::ObjectType::WALL, Pos(x+0.5,y+0.5)*cell_size, Vec2D(cell_size, cell_size)));

			auto &c = world.cells[y][x] = strategy::ObjectType::NONE;
			if(t == WALL)
				c = strategy::ObjectType::WALL;
			else if(t == PLATFORM)
				c = strategy::ObjectType::PLATFORM;
			else if(t == LADDER)
				c = strategy::ObjectType::LADDER;
			else if(t == JUMP_PAD)
				c = strategy::ObjectType::JUMP_PAD;

			//s += 'a' + (int)game.level.tiles[x][y];
		}
		//std::cerr << s << "\n";
	}

	int local_idx = 0;
	for(auto unit : game.units)
	{
		if(unit.playerId == my_unit.playerId) {
			//dprint(unit.jumpState.speed, unit.jumpState.canCancel, unit.jumpState.canJump);
		}

		strategy::Weapon weapon (Vec(-1,-1), Vec(-1,-1), strategy::WeaponType::NONE);
		if(unit.weapon)
		{
			weapon.weapon_type = convert_weapon_type(unit.weapon->typ);
			weapon.spread = unit.weapon->spread;
			real fire_timer = (unit.weapon->fireTimer.get() ? *unit.weapon->fireTimer : -1.0);
			weapon.fire_tick_left = floor(fire_timer * 60 + EPS);
			weapon.last_angle = unit.weapon->lastAngle ? *unit.weapon->lastAngle : 0.0;
			weapon.magazine = unit.weapon->magazine;
			assert((-PI-EPS < weapon.last_angle && weapon.last_angle < PI+EPS));
		}

		world.units.push_back(strategy::Unit(
			Pos(unit.position.x, unit.position.y+unit.size.y/2.0),
			Vec2D(unit.size.x, unit.size.y),
			Vec2D(0,0), 
			time_to_tick(unit.jumpState.maxTime), true/*unit.jumpState.canCancel && unit.jumpState.canJump*/, unit.jumpState.speed,
			unit.id, (int)world.units.size(), (unit.playerId == my_unit.playerId),
			unit.health,
			weapon
		));

		if(world.units.back().is_teammate)
		{
			world.units.back().local_idx = local_idx++;
		}

		world.units.back().on_ground = unit.onGround;
		world.units.back().nmine = unit.mines;

		// if(world.is_on_ground(world.units.back()) != unit.onGround)
		// {
		// 	dprint(world.tick, unit.onGround, world.units.back());
		// }
	}

	for(auto &bullet : game.bullets)
	{
		world.bullets.push_back( strategy::Bullet(
			Pos(bullet.position.x, bullet.position.y),
			Vec2D(bullet.size, bullet.size),
			Vec2D(bullet.velocity.x, bullet.velocity.y),
			convert_weapon_type(bullet.weaponType),
			bullet.damage,
			bullet.unitId
		));
		if(bullet.explosionParams)
		{
			world.bullets.back().exp_damage = bullet.explosionParams->damage;
			world.bullets.back().exp_size = bullet.explosionParams->radius;
		}
		

		strategy::Bullet b = world.bullets.back();
		assert(0.0 < b.x && b.x < world.X && 0.0 < b.y && b.y < world.Y);
		const int nsub_tick = 100;
		int t = 1;
		bool finish = false;
		while(!finish && -0.5 < b.x && b.x < world.X + 0.5 && -0.5 < b.y && b.y < world.Y + 0.5)
		{
			b += b.vel * (1.0/(60.0*nsub_tick));
			int iy1 = b.get_corner_celly(-1);
			int iy2 = b.get_corner_celly(1);
			int ix1 = b.get_corner_cellx(-1);
			int ix2 = b.get_corner_cellx(1);
			for(int iy=iy1; iy<=iy2; ++iy)
				for(int ix=ix1; ix<=ix2; ++ix)
					if(world.cells[iy][ix] == strategy::ObjectType::WALL)
					{
						world.bullets.back().last_tick = world.tick + t/nsub_tick;
						world.bullets.back().last_subtick = (t%nsub_tick)*1.0/nsub_tick;
						world.bullets.back().last_pos = b;
						finish = true;
					}
			t += 1;
		}
		assert(0.0 < b.x && b.x < world.X && 0.0 < b.y && b.y < world.Y);
	}

	for(auto &mine : game.mines)
	{
		world.mines.push_back(strategy::Mine(
			Pos(mine.position.x, mine.position.y+mine.size.y/2.0),
			Vec2D(mine.size.x + 2*mine.triggerRadius, mine.size.y + 2*mine.triggerRadius),
			(strategy::MineState)mine.state,
			mine.explosionParams.damage,
			mine.explosionParams.radius,
			mine.timer ? time_to_tick(*mine.timer) : 9999
		));
		dprint((int)mine.state);
		if(mine.timer) {
			dprint(*mine.timer);
		}
	}

	for(auto loot : game.lootBoxes)
	{
		Item *item = loot.item.get();
		if(dynamic_cast<Item::HealthPack*>(item))
		{
			world.heals.push_back(strategy::Object(strategy::ObjectType::HEAL, Pos(loot.position.x, loot.position.y+loot.size.y/2), Vec2D(loot.size.x, loot.size.y)));
		}
		else if(dynamic_cast<Item::Weapon*>(item))
		{
			strategy::WeaponType wt = convert_weapon_type(dynamic_cast<Item::Weapon*>(item)->weaponType);
			world.weapons.push_back(strategy::Weapon(Pos(loot.position.x, loot.position.y+loot.size.y/2), Vec2D(loot.size.x, loot.size.y), wt));
		}
		else if(dynamic_cast<Item::Mine*>(item))
		{
			world.loot_mines.push_back(strategy::Object(strategy::ObjectType::MINE, Pos(loot.position.x, loot.position.y+loot.size.y/2), Vec2D(loot.size.x, loot.size.y)));
		}
		//dprint(unit.playerId);
	}

	assert(game.players.size() == 2);
	for(int i=0; i<game.players.size(); ++i)
		world.scores[i] = game.players[i].score;

	if(my_unit.playerId != game.players[0].id)
		std::swap(world.scores[0], world.scores[1]);
}


UnitAction MyStrategy::getAction(const Unit &unit, const Game &game, Debug &debug)
{
	double t0 = get_time();

	if(time_left < 20.0)
	{
		fast_mode = true;
	}

	gdebug = &debug;
	
	if(game.currentTick != last_tick)
	{
		if(unit.weapon.get()) {
			Weapon wp = *unit.weapon.get();
			real fire_timer = (wp.fireTimer.get() ? *wp.fireTimer : -1.0);
			int fire_tick_left = floor(fire_timer * 60 + EPS);
			double angle = unit.weapon->lastAngle ? *unit.weapon->lastAngle : 0.0;
			dprint(game.currentTick, unit.health, wp.magazine, wp.spread, fire_tick_left, angle);
		} else {
			dprint(game.currentTick, unit.health);
		}
		if(game.currentTick == 0)
		{
			for(WeaponType wt : {PISTOL, ASSAULT_RIFLE, ROCKET_LAUNCHER})
			{
				auto st = (int)convert_weapon_type(wt);
				strategy::World::weapon_params[st].fire_delay = round(game.properties.weaponParams.at(wt).fireRate * 60);
				strategy::World::weapon_params[st].min_spread = game.properties.weaponParams.at(wt).minSpread;
				strategy::World::weapon_params[st].max_spread = game.properties.weaponParams.at(wt).maxSpread;
				strategy::World::weapon_params[st].recoil = game.properties.weaponParams.at(wt).recoil;
				strategy::World::weapon_params[st].aim_speed = game.properties.weaponParams.at(wt).aimSpeed;
				strategy::World::weapon_params[st].magazine_size = game.properties.weaponParams.at(wt).magazineSize;
				strategy::World::weapon_params[st].bullet_size = game.properties.weaponParams.at(wt).bullet.size;
				strategy::World::weapon_params[st].bullet_speed = game.properties.weaponParams.at(wt).bullet.speed;
				strategy::World::weapon_params[st].damage = game.properties.weaponParams.at(wt).bullet.damage;
				if(game.properties.weaponParams.at(wt).explosion) {
					strategy::World::weapon_params[st].exp_damage = game.properties.weaponParams.at(wt).explosion->damage;
					strategy::World::weapon_params[st].exp_size = game.properties.weaponParams.at(wt).explosion->radius;
				}

			}

			const auto &wp1 = game.properties.weaponParams.at(PISTOL);
			dprint(wp1.fireRate, wp1.reloadTime, wp1.minSpread, wp1.maxSpread, wp1.recoil, wp1.aimSpeed);
			dprint(wp1.bullet.speed, wp1.bullet.size, wp1.bullet.damage);
			const auto &wp2 = game.properties.weaponParams.at(ASSAULT_RIFLE);
			dprint(wp2.fireRate, wp2.reloadTime, wp2.minSpread, wp2.maxSpread, wp2.recoil, wp2.aimSpeed);
			dprint(wp2.bullet.speed, wp2.bullet.size, wp2.bullet.damage);
			const auto &wp3 = game.properties.weaponParams.at(ROCKET_LAUNCHER);
			dprint(wp3.fireRate, wp3.reloadTime, wp3.minSpread, wp3.maxSpread, wp3.recoil, wp3.aimSpeed);
			dprint(wp3.bullet.speed, wp3.bullet.size, wp3.bullet.damage);

			dprint(game.properties.unitMaxHorizontalSpeed, game.properties.unitFallSpeed, game.properties.unitJumpSpeed);
			dprint(game.properties.unitJumpTime, game.properties.jumpPadJumpTime, game.properties.jumpPadJumpSpeed);
		}
		strategy::World world;
		build_world(unit, game, world);
		//dprint(unit.position.x, unit.position.y, unit.playerId);

		for(int i=0; i<world.units.size(); ++i)
		{
			if(game.units[i].weapon.get())
			{
				real dx = (world[i].local_idx == 0 ? 0.0 : 8.0);
				bool on_left = world[i].is_teammate;
				strategy::draw_rect((on_left ? Vec(-2.0+dx,2.0) : Vec(42.0-dx,2.0)), Vec(0.4,game.units[i].weapon->spread*10.0), {0.0,1.0,0.0,1.0});
				char text[256];
				sprintf(text, "magazine: %d", game.units[i].weapon->magazine);
				strategy::draw_text((on_left ? Vec(-2.0+dx,1.0) : Vec(42.0-dx,1.0)), text, (on_left ? 0 : 2), 20.0, {1.0,1.0,1.0,1.0});
			}
		}

		solver.solve(world);

		// strategy::World w = world;
		// strategy::Action dbg_action;
		// dbg_action.vel = -99.0;
		// int unit_idx;
		// for(unit_idx=0; unit_idx<world.units.size(); ++unit_idx)
		// 	if(world[unit_idx].is_teammate)
		// 		break;
		// dprint(w[unit_idx].jump_speed);
		// for(int t=0; t<99; ++t)
		// {
		// 	dbg_action.jump = ((w.tick)/10%2);
		// 	w.simulate_tick(unit_idx, dbg_action);
		// 	draw_rect(w[unit_idx], Vec(0.1,0.1), {1.0,1.0,1.0,1.0});
		// }
		// dbg_action.jump = (world.tick/10%2);
		// solver.actions[unit.id] = dbg_action;
	}
	last_tick = game.currentTick;

	UnitAction action;
	action.velocity = solver.actions[unit.id].vel;
	action.jump = solver.actions[unit.id].jump;
	action.jumpDown = solver.actions[unit.id].jump_down;
	action.aim = Vec2Double(solver.actions[unit.id].aim.x, solver.actions[unit.id].aim.y);
	action.shoot = solver.actions[unit.id].shoot;
	action.reload = solver.actions[unit.id].reload;
	action.swapWeapon = solver.actions[unit.id].swap_weapon;
	action.plantMine = solver.actions[unit.id].plant_mine;

	double dt = get_time() - t0;
	time_left -= dt;
	time_spent += dt;

	return action;
}
