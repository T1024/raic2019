#ifndef _BASE_H_
#define _BASE_H_

#include "pos.h"
#ifdef _LOCAL
#	include "dbg_print.h"
#else
#	define dprint(...)
#endif

#ifdef _TIMING
#	include "dbg_timing.h"
#else
#	define TIMING_INIT(...)
#	define FUNCTION_STAT
#	define PRINT_STAT(...)
#endif

#define ASSERT assert

#define PI 3.1415926535897932384626433832795
#define EPS 1E-5

#ifdef _LOCAL
#	define LOCAL(a) a
#else
#	define LOCAL(a)
#endif

#define SQR(a) ((a)*(a))

typedef double real;
typedef Pos2T<real> Vec2D;
typedef Vec2D Pos;
typedef Vec2D Vec;
typedef Pos2T<int> Vec2I;

template<class T>
T clamp(T val, T min, T max)
{
	return std::min(std::max(min, val), max);
}

inline bool is_near(real a, real b)
{
	return fabs(a - b) < EPS;
}

inline bool is_near(const Vec2D &v1, const Vec2D &v2)
{
	return fabs(v1.x - v2.x) < EPS && fabs(v1.y - v2.y) < EPS;
}

inline real round(real x)
{
	return floor(x + 0.5);
}

#endif // _BASE_H_
