#ifndef _UTILS_H_
#define _UTILS_H_

#include <vector>
#include <assert.h>
#include "base.h"
#include "object.h"

class Debug;
extern Debug *gdebug;

namespace strategy
{

struct Object;
struct World;
struct Unit;

template<class T>
struct UnitsData
{
	T data[99];

	T &operator[] (const Unit &u) { assert(0 <= u.id && u.id < 99); return data[u.id]; }
	const T &operator[] (const Unit &u) const { assert(0 <= u.id && u.id < 99); return data[u.id]; }
};


struct Range
{
	real low, high;
	bool valid = false;

	Range() {}

	Range(real l, real h)
		: low(l)
		, high(h)
	{
		assert(low <= high);
		valid = true;
	}

	void expand(real x)
	{
		if(!valid)
		{
			low = high = x;
			valid = true;
			return;
		}

		low = std::min(low, x);
		high = std::max(high, x);
	}

	Range common(const Range &o) const
	{
		if(!valid || !o.valid)
		{
			assert(0);
			return Range();
		}

		if(high < o.low || o.high < low)
		{
			return Range();
		}

		return { std::max(low,o.low), std::min(high,o.high) };
	}

	real len() const
	{
		return valid ? high - low : 0.0;
	}
};


struct AngleRange : public Range
{
	AngleRange(const Range &range)
	{
		init(range.low, range.high);
	}

	AngleRange(real l, real h)
	{
		init(l, h);
	}

	void init(real l, real h)
	{
		while(h+EPS < l)
		{
			h += 2*PI;
		}

		while(l < -PI)
		{
			l += 2*PI;
			h += 2*PI;
		}

		low = l;
		high = h;
		valid = true;
	}

	real get_common_len(const AngleRange &r2)
	{
		const AngleRange &r1 = *this;
		real res = r1.common(r2).len();
		
		Range rr2 = r2;
		rr2.low -= 2*PI;
		rr2.high -= 2*PI;
		res = std::max(res, r1.common(rr2).len());

		rr2 = r2;
		rr2.low += 2*PI;
		rr2.high += 2*PI;
		res = std::max(res, r1.common(rr2).len());

		return res;
	}

	Range fit(const AngleRange &r2)
	{
		const AngleRange &r1 = *this;
		Range res = r2;
		real a = r1.common(r2).len();
		
		Range rr2 = r2;
		rr2.low -= 2*PI;
		rr2.high -= 2*PI;
		real b = r1.common(rr2).len();
		if(a < b)
		{
			a = b;
			res = rr2;
		}

		rr2 = r2;
		rr2.low += 2*PI;
		rr2.high += 2*PI;
		b = r1.common(rr2).len();
		if(a < b)
		{
			a = b;
			res = rr2;
		}

		return res;
	}

	bool contain(real x)
	{
		while(x < low)
			x += 2*PI;
		return x < high;
	}
};


class PathFinder
{
	struct PathCell
	{
		int cost = 999999;
		Vec2I from = Vec2I(-1,-1);
	};

	Table2T<PathCell,64,32> path_cell;

	static real interpolate(real c1, real c2, real q);

public:
	PathFinder() {}
	PathFinder(const World &world, int xo, int yo);

	bool build_path(Vec2I p1, Vec2I p2, std::vector<Vec2I> &path) const;

	real get_dist_from(const Pos &pos) const;
};


void draw_rect(Vec p, Vec size, const std::array<real,4> &color);
void draw_line(Vec p1, Vec p2, real width, const std::array<real,4> &color);
void draw_text(Vec p, const std::string &text, int aligment, real size, const std::array<real,4> &color);

inline int sign(real x) { return (x < 0.0 ? -1 : 1); }
inline real get_rect_dist(const Pos &p1, const Pos &p2) { return std::max(fabs(p1.x-p2.x), fabs(p1.y-p2.y)); }
bool is_intersect(const Object &o1, const Object &o2, real bound = 0.0);
bool is_intersect(const Object &o1, const Pos &o2, real radiusx, real radiusy);
bool is_line_intersect(const Pos &p11, const Pos &p12, const Pos &p21, const Pos &p22);
Pos get_first_intersect_wall(const World &world, const Pos &p0, const Vec2D &dir, const Vec2D &size);

real angle_diff(real a, real b);
Range get_view_range(const Pos &me, const Object &he, const Vec &dir0);

} // namespace strategy

#endif // _UTILS_H_
