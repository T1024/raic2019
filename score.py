#!/usr/bin/python3

import sys
import math
import time

def parse(name):
	file = open(name)
	scores = []
	s = [0,0]
	state = 0;
	for line in file:
		line = line.strip()
		if line.find("results") != -1:
			state = 3
		elif state == 2:
			s[state-1] = int(line[0:-1])
		elif state == 1:
			s[state-1] = int(line)
			scores.append(s[:])
		state -= 1
	file.close()
	return scores

def calc(scores, mod):
	res = [0,0,0]
	sc = [0,0]
	for s in scores:
		sc[0] += s[0] % mod
		sc[1] += s[1] % mod
		p1 = s[0] % mod
		p2 = s[1] % mod
		if p1 < p2:
			res[2] += 1
		elif p2 < p1:
			res[0] += 1
		else:
			res[1] += 1
	return res,sc

if len(sys.argv) < 2:
	quit ("Missed arguments: file1")

scores = parse(sys.argv[1])
r1,s1 = calc(scores, 1000000)
print("Stat:", r1, "Ratio:", r1[2] *1.0 / (r1[0]+r1[2]), "Scores: ", s1, s1[1] *1.0 / s1[0])
r1,s1 = calc(scores, 1000)
print("Stat:", r1, "Ratio:", r1[2] *1.0 / (r1[0]+r1[2]), "Scores: ", s1, s1[1] *1.0 / s1[0])
