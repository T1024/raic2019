#! /usr/bin/python3

import os
import subprocess
import sys
import json

def build_args(config, vis, save_results, save_replay, id, additional):
	runner = "Runner/aicup2019"
	args = [runner, "--config", config]
	if not vis:
		args.append("--batch-mode")
	if 0 < len(save_results):
		args.append("--save-results")
		args.append(save_results)
	if 0 < len(save_replay):
		args.append("--save-replay")
		args.append(save_replay)
	for a in additional:
		args.append(a)
	return args


def build_config_file(id, seed, team_size, player1, player2, level):
	level_conf = "Complex"
	if 0 < len(level):
		level_conf = {"LoadFrom": {"path": "levels/" + level}}

	config = {
		"seed": seed,
		"options_preset": {
			"Custom": {
				"level": level_conf,
				"properties": {
					"max_tick_count": 3600, # modified!
					"team_size": team_size,
					"ticks_per_second": 60.0,
					"updates_per_tick": 100,
					"loot_box_size": {
						"x": 0.5,
						"y": 0.5
					},
					"unit_size": {
						"x": 0.9,
						"y": 1.8
					},
					"unit_max_horizontal_speed": 10.0,
					"unit_fall_speed": 10.0,
					"unit_jump_time": 0.55,
					"unit_jump_speed": 10.0,
					"jump_pad_jump_time": 0.525,
					"jump_pad_jump_speed": 20.0,
					"unit_max_health": 100,
					"health_pack_health": 50,
					"weapon_params": {
						"RocketLauncher": {
							"magazine_size": 1,
							"fire_rate": 1.0,
							"reload_time": 1.0,
							"min_spread": 0.1,
							"max_spread": 0.5,
							"recoil": 1.0,
							"aim_speed": 1.0,
							"bullet": {
								"speed": 20.0,
								"size": 0.4,
								"damage": 30
							},
							"explosion": {
								"radius": 3.0,
								"damage": 50
							}
						},
						"Pistol": {
							"magazine_size": 8,
							"fire_rate": 0.4,
							"reload_time": 1.0,
							"min_spread": 0.05,
							"max_spread": 0.5,
							"recoil": 0.5,
							"aim_speed": 1.0,
							"bullet": {
								"speed": 50.0,
								"size": 0.2,
								"damage": 20
							},
							"explosion": None
						},
						"AssaultRifle": {
							"magazine_size": 20,
							"fire_rate": 0.1,
							"reload_time": 1.0,
							"min_spread": 0.1,
							"max_spread": 0.5,
							"recoil": 0.2,
							"aim_speed": 1.9,
							"bullet": {
								"speed": 50.0,
								"size": 0.2,
								"damage": 5
							},
							"explosion": None
						}
					},
					"mine_size": {
						"x": 0.5,
						"y": 0.5
					},
					"mine_explosion_params": {
						"radius": 3.0,
						"damage": 50
					},
					"mine_prepare_time": 1.0,
					"mine_trigger_time": 0.5,
					"mine_trigger_radius": 1.0,
					"kill_score": 1000
				}
			}
		},
		"players": [
			{
				"Tcp": {
					"host": None,
					"port": player1,
					"accept_timeout": None,
					"timeout": None,
					"token": None
				}
			},
			{
				"Tcp": {
					"host": None,
					"port": player2,
					"accept_timeout": None,
					"timeout": None,
					"token": None
				}
			}
		]
	}

	file_name = 'tmp' + chr(ord('A') + id) + '.json'
	fconfig = open('Runner/' + file_name, 'w')
	json.dump(config, fconfig)
	fconfig.close()
	return file_name

def run(args):
	print(args)
	return subprocess.Popen(args)

mode = 0
seed0 = None
team_size = 1
test_num = 100
vis = True
level = ''
players = []
print(sys.platform)
if sys.platform.find('x') != -1:
	exe = 'a'
	ref = './45ebc3'
else:
	exe = 'a'
	ref = '523d63.exe'

other_args = []
skip_next = False
for i in range(1, len(sys.argv)):
	if skip_next:
		skip_next = False
		continue
	if i+1 < len(sys.argv) and sys.argv[i] == '--exe':
		exe = sys.argv[i+1]
		players.append(exe)
		skip_next = True
	elif i+1 < len(sys.argv) and sys.argv[i] == '--ref':
		ref = sys.argv[i+1]
		players.append(ref)
		skip_next = True
	elif sys.argv[i] == '--novis':
		vis = False
	elif sys.argv[i] == '--ref-test':
		mode = 1
	elif i+1 < len(sys.argv) and sys.argv[i] == '--batch-test':
		mode = 2
		test_num = int(sys.argv[i+1])
		skip_next = True
	elif i+1 < len(sys.argv) and sys.argv[i] == '--seed':
		seed0 = int(sys.argv[i+1])
		skip_next = True
	elif i+1 < len(sys.argv) and sys.argv[i] == '--team':
		team_size = int(sys.argv[i+1])
		skip_next = True
	elif i+1 < len(sys.argv) and sys.argv[i] == '--level':
		level = sys.argv[i+1]
		skip_next = True
	else:
		other_args.append(sys.argv[i])
print(other_args)

if len(players) < 2:
	players = [exe, ref]

make_cmd = 'make EXE=' + exe
if mode == 0 and vis:
	make_cmd += ' VIS=1'

if os.system(make_cmd):
	quit()

id = ord(exe[0]) - ord('a')
port1 = 31001+2*id
port2 = 31001+2*id+1
print(id, port1, port2)

if mode == 1:
	for seed in range(1,6):
		config = build_config_file(id, seed, team_size, port1, port2, level)
		output = subprocess.Popen(build_args(config, False, "", 'test'+str(seed) + '.rpl', id, other_args))
		run(['sleep', '0.1']).communicate()
		run(['./'+exe, str(port1)])
		run(['./'+exe, str(port2)])
		output.communicate()

elif mode == 2:
	start_seed = 0 if seed0 == None else seed0
	for seed in range(start_seed, start_seed + test_num):
		print(seed)
		config = build_config_file(id, seed, team_size, port1, port2, level)
		res_file = chr(ord('a')+id) + '.txt'
		output = run(build_args(config, False, res_file, '', id, other_args))
		run(['sleep', '0.2']).communicate()
		run(['./'+players[0], str(port1)])
		run(['./'+players[1], str(port2)])
		output.communicate()
		os.system('cat Runner/' + res_file + ' >> ' + 'res' + chr(ord('A') + id) + '.txt')

else:
	config = build_config_file(id, seed0, team_size, port1, port2, level)
	output = subprocess.Popen(build_args(config, vis, "", "", id, other_args))
	run(['sleep', '0.2']).communicate()
	run(['./'+players[0], str(port1)])
	run(['./'+players[1], str(port2)])
	output.communicate()

